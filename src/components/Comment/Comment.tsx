import "./Comment.css";

export function Comment({ comment }) {
  return (
    <div className="comment-container">
      <img
        className="comments-profile-pic"
        src={
          "https://www.pngitem.com/pimgs/m/3-30711_sesame-street-elmo-face-hd-png-download.png"
        }
        alt=""
      ></img>
      <span className="comment-input-text">{comment}</span>
    </div>
  );
}

export default Comment;
