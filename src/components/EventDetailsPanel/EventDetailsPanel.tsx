import React from "react";
import { Link } from "react-router-dom";
import {
  calculateDaysLeft,
  isEventInPast,
  unixTimestampToReadableDateAndTime,
} from "../../helpers/utils";
import "./EventDetailsPanel.css";

export function EventDetailsPanel({ eventDetails }) {
  const [eventIsPast] = React.useState(false);

  React.useEffect(() => {
    isEventInPast(eventDetails.startTimestamp);
  }, [eventDetails]);

  return (
    <div className="event-details-wrapper">
      <div className="event-details">
        <div className="event-details-header">
          <div className="event-details-header-top">
            {/* {!authJwt && (<h1>DON'T HAVE AN ACCOUNT? GET ONE TODAY HAHA</h1>)} */}
            {/* {eventIsPast && (<h4>this is an old event</h4>)} */}
            <div className="event-title-container">
              <h1 className="event-title">{eventDetails.title}</h1>
            </div>
            <Link className="nav-item" to="/profile">
              <div className="host-name-container">
                <div className="host-name">{eventDetails.adminFirstName}</div>
                <div className="host-name">{eventDetails.adminLastName}</div>
              </div>
              <img
                className="nav-profile-pic"
                src={eventDetails.adminProfilePicUrl}
                alt=""
              ></img>
            </Link>
          </div>
          <div className="event-details-header-bottom">
            <div className="start-time-end-time">
              {unixTimestampToReadableDateAndTime(eventDetails.startTimestamp)}{" "}
              - {unixTimestampToReadableDateAndTime(eventDetails.endTimestamp)}
            </div>
            <div className="days-left-container">
              {calculateDaysLeft(eventDetails.startTimestamp, eventIsPast)}
            </div>
            {eventDetails.isPrivate && (
              <div className="is-private-container">🔒 Private</div>
            )}
          </div>
        </div>
        <div className="description-container">
          <div
            className="event-image-container"
            style={{
              backgroundImage: 'url("' + eventDetails.eventPicUrl + '")',
            }}
          ></div>
          {eventDetails.description}
        </div>
        {/* <div className="tags-container">
                  <>Tags: {eventDetails.tags}</>
                </div> */}
      </div>
    </div>
  );
}

export default EventDetailsPanel;
