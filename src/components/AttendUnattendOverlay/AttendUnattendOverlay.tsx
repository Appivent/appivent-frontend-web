import "./AttendUnattendOverlay.css";
import Button from "../Button/Button";
import { useJoinEvent, useLeaveEvent } from "../../hooks/event/use-event";

export function AttendUnattendOverlay({
  authUserId,
  eventDetails,
  isAttendingEvent,
  setShowAttendUnattendOverlay,
}) {
  const { mutate: joinEventMutation } = useJoinEvent({
    lifecycleNotifications: {
      success: "Joined Event",
      error: "Unable to join",
    },
  });
  const { mutate: leaveEventMutation } = useLeaveEvent({
    lifecycleNotifications: {
      success: "No longer attending",
      error: "Unable to leave",
    },
  });
  const joinOrLeave = async (join) => {
    if (join) {
      joinEventMutation({ query: { eventId: eventDetails.eventId } });
    } else {
      leaveEventMutation({
        query: { eventId: eventDetails.eventId, userId: authUserId },
      });
    }
    setShowAttendUnattendOverlay(false);
  };

  return (
    <div className="overlay-container">
      <div
        className="overlay"
        onClick={() => {
          setShowAttendUnattendOverlay(false);
        }}
      ></div>
      <div className="welcome-overlay-container">
        <h5>{isAttendingEvent ? "Leave Event?" : "Join Event?"}</h5>
        <div className="yes-no-buttons-container">
          <Button
            text={"Yes"}
            styleOverrides={{
              backgroundColor: "var(--light-blue)",
              margin: "0 5px",
            }}
            onClickCallback={function () {
              if (isAttendingEvent) {
                joinOrLeave(false);
              } else {
                joinOrLeave(true);
              }
            }}
          ></Button>
          <Button
            text={"No"}
            styleOverrides={{
              backgroundColor: "var(--dark-orange)",
              margin: "0 5px",
            }}
            onClickCallback={function () {
              setShowAttendUnattendOverlay(false);
            }}
          ></Button>
        </div>
      </div>
    </div>
  );
}

export default AttendUnattendOverlay;
