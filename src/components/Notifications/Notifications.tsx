import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./Notifications.css";
import Button from "../Button/Button";
import BellIcon from "../../assets/bell.svg";
import useComponentVisible from "./hooks/useComponentVisible";
import {
  useGetNotifications,
  useRespondFriendRequest,
} from "../../hooks/user/use-user";
import { useRsvpEvent } from "../../hooks/event/use-event";

export const Notifications = () => {
  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);
  const [toggleBell, setToggleBell] = useState(true);
  const [notificationsCounter, setNotificationsCounter] = useState(0);
  const history = useHistory();

  const { data: notifications, refetch: refetchGetNotifications } = useGetNotifications({
    lifecycleNotifications: { error: "Unable to search friends" },
    onSuccessCallback: (res) => {
      setNotificationsCounter(
        res.eventNotifications.length + res.userNotifications.length
      );
    },
  });

  const { mutate: responseEventInviteMutation } = useRsvpEvent({
    lifecycleNotifications: {
      success: "Succesfully responded",
      error: "Unable to respond",
    },
  });

  const { mutate: responseFriendRequestMutation } = useRespondFriendRequest({
    lifecycleNotifications: {
      success: "Succesfully responded",
      error: "Unable to respond",
    },
  });

  React.useEffect(() => {
    const initNotificationsPoller = async () => {
      //TODO: uncomment this
      // setInterval(() => {
      refetchGetNotifications();
      // }, 5000)
    };
    initNotificationsPoller();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const respondFriendNotification = async (userId, accepted) => {
    responseFriendRequestMutation({ query: { userId, accepted } });
  };

  const respondEventNotification = async (eventId, accepted) => {
    responseEventInviteMutation({ query: { eventId, accepted } });
  };

  const toggleDropdown = () => {
    setToggleBell(false);
    setIsComponentVisible(!isComponentVisible);
  };

  const onProfileClick = (userId) => {
    history.push({
      pathname: "/profile",
      search: "?userId=" + userId,
    });
    toggleDropdown();
  };

  const onUserEventClick = (eventId) => {
    history.push({
      pathname: "/event",
      search: "?eventId=" + eventId + "&mode=view",
    });
    toggleDropdown();
  };

  return (
    <>
      {
        <div className={"notifications-wrapper"} ref={ref}>
          <img
            src={BellIcon}
            alt="Bell Icon"
            className="bell-icon"
            onClick={() => toggleDropdown()}
          ></img>

          {toggleBell && notificationsCounter > 0 && (
            <span
              className="badge"
              onClick={() => setIsComponentVisible(!isComponentVisible)}
            >
              {notificationsCounter}
            </span>
          )}

          {isComponentVisible && (
            <>
              <div className="notification-panel-arrow"></div>
              <div className="notification-panel-container">
                <h2 className="notification-title">Notifications</h2>
                {notificationsCounter > 0 && (
                  <>
                    {notifications.userNotifications.map((notification, i) => {
                      return (
                        <div key={i} className="notification-container">
                          <div className="notification-img-msg">
                            <img
                              src={notification.profilePicUrl}
                              className="notification-profile-img"
                              alt="Profile"
                              onClick={() =>
                                onProfileClick(notification.userId)
                              }
                            ></img>

                            <div className="notification-message-buttons">
                              <p>
                                <span
                                  className="notification-profile-name-link"
                                  onClick={() =>
                                    onProfileClick(notification.userId)
                                  }
                                >
                                  {notification.firstName}{" "}
                                  {notification.lastName}{" "}
                                </span>
                                <br />
                                sent you a friend request!
                              </p>
                              <div className="notification-button-wrapper">
                                <Button
                                  text="Accept"
                                  styleOverrides={{
                                    backgroundColor: "var(--button-blue)",
                                    padding: "5px 12px",
                                    margin: "0 10px 0 0",
                                  }}
                                  onClickCallback={() => {
                                    respondFriendNotification(
                                      notification.userId,
                                      true
                                    );
                                  }}
                                />
                                <Button
                                  text="Decline"
                                  styleOverrides={{
                                    backgroundColor: "var(--dark-orange)",
                                    padding: "5px 12px",
                                  }}
                                  onClickCallback={() => {
                                    respondFriendNotification(
                                      notification.userId,
                                      false
                                    );
                                  }}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}

                    {notifications.eventNotifications.map((notification, i) => {
                      return (
                        <div key={i} className="notification-container">
                          <div className="notification-img-msg">
                            <img
                              src={notification.eventPicUrl}
                              className="notification-profile-img"
                              alt="Profile"
                              onClick={() =>
                                onUserEventClick(notification.eventId)
                              }
                            ></img>

                            <div className="notification-message-buttons">
                              <p>You have been invited to</p>
                              <span
                                className="notification-profile-name-link"
                                onClick={() =>
                                  onUserEventClick(notification.eventId)
                                }
                              >
                                {" " + notification.title}
                              </span>
                              <div className="notification-button-wrapper">
                                <Button
                                  text="Accept"
                                  styleOverrides={{
                                    backgroundColor: "var(--button-blue)",
                                    padding: "5px 12px",
                                    margin: "0 10px 0 0",
                                  }}
                                  onClickCallback={() => {
                                    respondEventNotification(
                                      notification.eventId,
                                      true
                                    );
                                  }}
                                />
                                <Button
                                  text="Decline"
                                  styleOverrides={{
                                    backgroundColor: "var(--dark-orange)",
                                    padding: "5px 12px",
                                  }}
                                  onClickCallback={() => {
                                    respondEventNotification(
                                      notification.eventId,
                                      false
                                    );
                                  }}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </>
                )}
                {notificationsCounter <= 0 && (
                  <div className="no-new-notifications">
                    No new notifications
                  </div>
                )}
              </div>
            </>
          )}
        </div>
      }
    </>
  );
};
