import "./EventDetailsLocationPanel.css";

export function EventDetailsLocationPanel({ eventDetails }) {
  return (
    <div className="event-directions-wrapper">
      <h3>Directions</h3>
      <div className="location-container">
        <div className="location-name">📍 {eventDetails.locationName}</div>
        {eventDetails.locationPlaceId && (
          <div className="map-container">
            <iframe
              width="100%"
              height="250"
              frameBorder="0"
              style={{ border: "0" }}
              allowFullScreen
              src={
                "https://www.google.com/maps/embed/v1/place?key=AIzaSyCDR3zAcLM2MSLuZ0qm0hjkudlU4VarZKs&q=place_id:" +
                eventDetails.locationPlaceId
              }
            ></iframe>
          </div>
        )}
      </div>
    </div>
  );
}

export default EventDetailsLocationPanel;
