import Comment from "../Comment/Comment";
import CommentBar from "../CommentBar/CommentBar";
import "./CommentSection.css";

export function CommentSection({ comments }) {
  return (
    <>
      <CommentBar></CommentBar>
      {comments?.map((comment) => 
        <Comment comment={comment}></Comment>
      )}
      <span className="view-more-comments">View 3 more comments</span>
    </>
  );
}

export default CommentSection;
