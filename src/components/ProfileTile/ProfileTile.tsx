import "./ProfileTile.css";
import { useHistory } from "react-router-dom";
import RemoveCircle from "../../assets/remove-circle.svg";

export default function ProfileTile({
  user,
  containerStyleOverrides = {},
  imageStyleOverrides = {},
  spanStyleOverrides = {},
  onXClicked,
}) {
  const history = useHistory();

  const onProfileClick = () => {
    history.push({
      pathname: "/profile",
      search: "?userId=" + user.userId,
    });
  };

  return (
    <>
      {onXClicked && (
        <div className="profile-tile-remove-user-container">
          <img
            src={RemoveCircle}
            alt="Remove User"
            className="profile-tile-remove-user"
            onClick={() => {
              onXClicked(user.userId);
            }}
          ></img>
        </div>
      )}
      <div
        style={{ ...containerStyleOverrides }}
        className="profile-tile-container"
        onClick={onProfileClick}
      >
        <img
          style={{ ...imageStyleOverrides }}
          className="tile-profile-pic"
          src={user.profilePicUrl}
        ></img>
        <span style={{ ...spanStyleOverrides }} className="profile-tile-name">
          {user.firstName + " " + user.lastName}
        </span>
      </div>
    </>
  );
}
