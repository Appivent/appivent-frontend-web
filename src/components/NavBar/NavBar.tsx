import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { SignInOverlay } from "../../components/SignInOverlay/SignInOverlay";
import "./NavBar.css";
import logo from "../../assets/appivent-logo.png";
import { Notifications } from "../../components/Notifications/Notifications";
import { useAuth } from "../../hooks/use-auth";
import { useGetUser } from "../../hooks/user/use-user";

export const NavBar = () => {
  const auth = useAuth();
  const history = useHistory();

  const [userId, setUserId] = useState("");
  const [toggleSignInOverlay, setToggleSignInOverlay] = useState(false);

  const { data: profileData, refetch: refetchSelectedUserProfile } = useGetUser(
    {
      lifecycleNotifications: { error: "Unable to find user details" },
      query: { userId },
    }
  );

  useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      const thisUserId = auth.userId;

      if (thisAuthJwt) {
        setUserId(thisUserId);
      }
    };
    getAuthInfo();
  }, []);

  useEffect(() => {
    !toggleSignInOverlay && enableVerticalScroll();
  }, [toggleSignInOverlay]);

  const navigate = (route) => {
    const currentRoute = history.location.pathname + history.location.search;
    if (route !== currentRoute) {
      history.push(route);
    }
  };

  const toggleSignIn = () => {
    setToggleSignInOverlay(!toggleSignInOverlay);
  };

  const enableVerticalScroll = () => {
    document.body.style.overflowY = "scroll";
  };

  return (
    <>
      <div>
        <div className="nav-bar-container">
          <div className="nav-container-left">
            <img className="nav-logo" src={logo} alt="Appivent Logo"></img>
            <h1 className="nav-title">Appivent</h1>
          </div>
          <div className="nav-container-right">
            {auth.isLoggedIn ? (
              <>
                <div
                  className="nav-item"
                  onClick={() => {
                    navigate("/home?userId=" + userId);
                  }}
                >
                  Home
                </div>
                <div
                  className="nav-item"
                  onClick={() => {
                    navigate("/explore");
                  }}
                >
                  Explore
                </div>
                <div
                  className="nav-item"
                  onClick={() => {
                    navigate("/friends");
                  }}
                >
                  Friends
                </div>

                <Notifications />
                <div
                  className="nav-item"
                  onClick={() => {
                    navigate("/profile");
                  }}
                >
                  <img
                    className="nav-profile-pic"
                    src={profileData?.profilePicUrl}
                    alt=""
                  ></img>
                </div>
              </>
            ) : (
              <>
                <div
                  className="nav-item"
                  onClick={() => {
                    navigate("/explore");
                  }}
                >
                  Explore
                </div>
                <button
                  className="signin-button"
                  onClick={() => {
                    toggleSignIn();
                  }}
                >
                  Sign In
                </button>
              </>
            )}
          </div>
        </div>
      </div>

      {toggleSignInOverlay && <SignInOverlay toggleSignIn={toggleSignIn} />}
    </>
  );
};
