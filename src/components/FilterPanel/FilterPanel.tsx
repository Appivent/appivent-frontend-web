import "./FilterPanel.css";
import Button from "../Button/Button";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import React, { useRef } from "react";
import { isSameDate } from "../../helpers/utils";
import Autocomplete from "react-autocomplete";
import { useGetLocations } from "../../hooks/event/use-event";
import useDebounce from "../../hooks/use-debounce";

const autoCompleteMenuStyle = {
  borderRadius: "3px",
  boxShadow: "0 2px 12px rgba(0, 0, 0, 0.1)",
  background: "rgba(255, 255, 255, 0.9)",
  padding: "0",
  position: "fixed",
  overflow: "auto",
  maxHeight: "50%",
  zIndex: 99,
};

export default function FilterPanel({
  setLocationFilterQuery,
  setTagsFilterQuery,
  setFromDateFilterQuery,
  setToDateFilterQuery,
  tagsFilterQuery,
  fromDateFilterQuery,
  toDateFilterQuery,
  totalEvents,
  clearFilters,
}) {
  let isFirstRender = useRef(true);
  const [tagQuery, setTagQuery] = React.useState("");
  const [locationAutocomplteQuery, setLocationAutocomplteQuery] =
    React.useState("");
  const [isLocationAutocompleteOpen, setIsLocationAutocompleteOpen] =
    React.useState(false);

  const debouncedSearchQuery = useDebounce(locationAutocomplteQuery, 600);
  const { data: locationsData, refetch: refetchListLocations } =
    useGetLocations({
      lifecycleNotifications: { error: "Unable to list locations" },
      query: { query: debouncedSearchQuery },
    });

  const addTag = () => {
    if (tagQuery) {
      setTagsFilterQuery([...tagsFilterQuery, tagQuery]);
      setTagQuery("");
    }
  };

  const handleTagEnter = (key) => {
    if (key === "Enter") {
      addTag();
    }
  };

  const removeTag = (tagToRemove) => {
    setTagsFilterQuery(tagsFilterQuery.filter((e) => e !== tagToRemove));
  };

  // Hack used to detect on autocomplete close to do a search for custom location
  React.useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
    } else {
      if (!isLocationAutocompleteOpen) {
        setLocationFilterQuery(locationAutocomplteQuery);
      }
    }
  }, [isLocationAutocompleteOpen]);

  const searchForLocation = async (query) => {
    setLocationAutocomplteQuery(query);
    if (query.length > 3) {
      refetchListLocations();
    }
  };

  return (
    <div className="filter-panel-container">
      <div className="filter-panel">
        {(locationAutocomplteQuery ||
          tagsFilterQuery.length > 0 ||
          toDateFilterQuery ||
          fromDateFilterQuery) &&
          !isSameDate(fromDateFilterQuery, new Date()) && (
            <Button
              text={"Clear Filters"}
              styleOverrides={{
                backgroundColor: "var(--light-orange)",
                marginRight: "5px",
              }}
              onClickCallback={clearFilters}
            />
          )}

        <div className="filter-type-container">
          <span className="filter-panel-text">Location:</span>
          {/* <input className="filter-item-container" placeholder={"Location"} value={locationFilterQuery || ""} onChange={e => { setLocationFilterQuery(e.target.value); }} /> */}
          <Autocomplete
            renderInput={(props) => {
              return <input className="filter-item-container" {...props} />;
            }}
            menuStyle={autoCompleteMenuStyle}
            wrapperStyle={{ display: "inline-block" }}
            getItemValue={(googleLocation) => googleLocation.description}
            items={locationsData || []}
            renderItem={(googleLocation, isHighlighted) => (
              <div
                style={{
                  background: isHighlighted ? "var(--light-blue)" : "white",
                  padding: "5px",
                }}
              >
                {googleLocation.description}
              </div>
            )}
            value={locationAutocomplteQuery}
            onChange={(e) => {
              searchForLocation(e.target.value);
            }}
            onSelect={(description, googleLocation) => {
              setLocationAutocomplteQuery(description);
              setLocationFilterQuery(description);
            }}
            onMenuVisibilityChange={() => {
              setIsLocationAutocompleteOpen(!isLocationAutocompleteOpen);
            }}
          />
        </div>

        <div className="filter-type-container">
          <span className="filter-panel-text">Start:</span>
          <DatePicker
            className="filter-item-container"
            selected={fromDateFilterQuery}
            onChange={(date) => setFromDateFilterQuery(date)}
            selectsStart
            startDate={fromDateFilterQuery}
            endDate={toDateFilterQuery}
            dateFormat="dd/MM/yyyy"
            showYearDropdown
            dateFormatCalendar="MMMM"
            yearDropdownItemNumber={15}
            scrollableYearDropdown
            placeholderText="📅 Start Date"
          />
        </div>

        <div className="filter-type-container">
          <span className="filter-panel-text">End:</span>
          <DatePicker
            className="filter-item-container"
            selected={toDateFilterQuery}
            onChange={(date) => setToDateFilterQuery(date)}
            selectsEnd
            startDate={fromDateFilterQuery}
            endDate={toDateFilterQuery}
            minDate={fromDateFilterQuery}
            dateFormat="dd/MM/yyyy"
            showYearDropdown
            dateFormatCalendar="MMMM"
            yearDropdownItemNumber={15}
            scrollableYearDropdown
            placeholderText="📅 End Date"
          />
        </div>

        <div className="filter-type-container">
          <span className="filter-panel-text">Tags:</span>
          <div style={{ display: "inline-block" }}>
            <input
              className="filter-item-container"
              placeholder={"Submit to add"}
              value={tagQuery || ""}
              onKeyDown={(e) => {
                handleTagEnter(e.key);
              }}
              onChange={(e) => {
                setTagQuery(e.target.value);
              }}
            />
            <div className="filter-tags-container">
              {tagsFilterQuery.map((tag) => {
                return (
                  <Button
                    text={tag}
                    styleOverrides={{
                      backgroundColor: "var(--light-orange)",
                      margin: "2px 2px",
                    }}
                    onClickCallback={() => {
                      removeTag(tag);
                    }}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
