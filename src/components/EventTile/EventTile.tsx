import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { calculateDaysLeft, isEventInPast } from "../../helpers/utils";
import CommentBar from "../CommentBar/CommentBar";
import CommentSection from "../CommentSection/CommentSection";
import "./EventTile.css";

export function EventTile({
  showComments = false,
  styleOverrides = {},
  onClickCallback,
  eventDetails,
}) {
  const eventComments = [];
  const [eventIsPast, setEventIsPast] = React.useState(
    isEventInPast(eventDetails.startTimestamp)
  );

  return (
    <div className="event-tile">
      <div
        className="event-tile-container"
        onClick={() => {
          onClickCallback(eventDetails.eventId);
        }}
      >
        <div
          className="event-tile-image-container"
          style={{ backgroundImage: 'url("' + eventDetails.eventPicUrl + '")' }}
        ></div>
        <div className="event-tile-details">
          <div className="event-tile-details-top-row">
            <span className="event-tile-title">{eventDetails.title}</span>
            <span className="event-tile-location">
              {eventDetails.locationName}
            </span>
          </div>

          <div className="event-tile-details-middle-row">
            <span className="event-tile-start-date">
              {new Date(eventDetails.startTimestamp * 1000).toDateString()}
            </span>
            <span className="event-tile-days-left">
              {calculateDaysLeft(eventDetails.startTimestamp, eventIsPast)}
            </span>
          </div>

          <div className="event-tile-details-bottom-row">
            {eventDetails.numAttending > 1 ? (
              <span className="event-tile-people-going">
                👤 {eventDetails.numAttending} people going
              </span>
            ) : (
              <span className="event-tile-people-going">👤 1 person going</span>
            )}
          </div>
        </div>
      </div>
      {showComments && (
        <div className="event-tile-comments-container">
          <CommentSection comments={eventComments}></CommentSection>
        </div>
      )}
    </div>
  );
}

export default EventTile;
