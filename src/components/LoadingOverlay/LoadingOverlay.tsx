import './LoadingOverlay.css';

export function LoadingOverlay({ text }) {
  return (
    <div className="loading-container">
      <div className="spinner">
        <div className="rect1"></div>
        <div className="rect2"></div>
        <div className="rect3"></div>
        <div className="rect4"></div>
        <div className="rect5"></div>
      </div>
      <span className="loading-overlay-text">{text}</span>
    </div>
  );
}

export default LoadingOverlay;
