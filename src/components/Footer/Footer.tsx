import React from 'react';
import './Footer.css';
import { Link } from "react-router-dom";
import logo from "../../assets/appivent-logo.png";
import FacebookIcon from "../../assets/facebook.svg";
import InstagramIcon from "../../assets/instagram.svg";
import LinkedInIcon from "../../assets/linkedin.svg";
import SnapchatIcon from "../../assets/snapchat.svg";
import TikTokIcon from "../../assets/tik-tok.svg";
import TwitterIcon from "../../assets/twitter.svg";

export function Footer() {

  return (
    <footer>
      <div className="footer-top-section">
        <img className="footer-logo" src={logo} alt="Appivent Logo"></img>
        <div className="footer-links">
          <a href="http://google.com">Home</a>
          <a href="http://google.com">Explore</a>
          <a href="http://google.com">Friends</a>
        </div>
        <div className="footer-links">
          <a href="http://google.com">Profile</a>
          <a href="http://google.com">Terms & Conditions</a>
          <a href="http://google.com">Privacy Policy</a>
        </div>
        <div className="footer-links">
          <a href="http://google.com">Contact</a>
          <a href="http://google.com">Investor Relations</a>
          <a href="http://google.com">Careers</a>
        </div>
      </div>
      <div className="footer-bottom-section">
        <div className="footer-social-media-links">
          <Link to="https://www.facebook.com/Appivent">
            <img
              src={FacebookIcon}
              alt="Facebook"
              className="footer-social-media-icon"
            ></img>
          </Link>
          <Link to="https://www.instagram.com/appivent/">
            <img
              src={InstagramIcon}
              alt="Instagram"
              className="footer-social-media-icon"
            ></img>
          </Link>
          <Link to="https://www.linkedin.com/company/appivent">
            <img
              src={LinkedInIcon}
              alt="LinkedIn"
              className="footer-social-media-icon"
            ></img>
          </Link>
          <Link to="https://www.tiktok.com/@appivent">
            <img
              src={TikTokIcon}
              alt="TikTok"
              className="footer-social-media-icon"
            ></img>
          </Link>
          <Link to="https://twitter.com/AppiventCompany">
            <img
              src={TwitterIcon}
              alt="Twitter"
              className="footer-social-media-icon"
            ></img>
          </Link>
        </div>
        <div className="footer-copyright-text">
          {/* <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div> */}
          <span>© 2021 Appivent. All rights reserved.</span>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
