import { useSelector, useDispatch } from "react-redux";
import CommentSection from "../CommentSection/CommentSection";
import "./EventDetailsCommentsPanel.css";

export function EventDetailsCommentsPanel({ eventDetails }) {
  const eventComments = [];

  return (
    <div className="event-comments-wrapper">
      <h3>Comments</h3>
      {eventComments.length > 0 && (
        <CommentSection comments={eventComments}></CommentSection>
      )}
    </div>
  );
}

export default EventDetailsCommentsPanel;
