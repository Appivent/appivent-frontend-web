import "./CommentBar.css";
import { useSelector } from "react-redux";
import { useState } from "react";

export function CommentBar() {
  const personalProfile = useSelector(
    (state) => state.user.personalProfile.userDetails
  );
  const [comment, setComment] = useState("Hey, this event sux");

  return (
    <div className="comment-bar-container">
      <img
        className="comments-bar-profile-pic"
        src={personalProfile.profilePicUrl}
        alt=""
      ></img>
      <input
        className="comment-bar-input-text"
        type="email"
        value={comment}
        onChange={(event) => setComment(event.target.value)}
        placeholder="Enter comment..."
        required
      />
    </div>
  );
}

export default CommentBar;
