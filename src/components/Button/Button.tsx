import './Button.css';

export function Button({ styleOverrides, onClickCallback, text }) {
  return (
    <button style={styleOverrides} className={"custom-button" } onClick={onClickCallback}>{text}</button>
  );
}

export default Button;
