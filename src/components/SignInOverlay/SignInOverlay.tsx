import { useState, useEffect, useRef } from "react";
import { validateEmail, validatePassword } from "../../helpers/validation";
import "./SignInOverlay.css";
import Button from "../Button/Button";
import { useHistory } from "react-router-dom";
import { dateToDdmmyyyyString } from "../../helpers/utils";
import { useSelector, useDispatch } from "react-redux";
import { SignInOverlayModes } from "../../store/storeTypes";
import ArrowLeft from "../../assets/arrow-left.svg";
import CheckCircle from "../../assets/check-circle.svg";
import { useAuth } from "../../hooks/use-auth";

export const SignInOverlay = ({ toggleSignIn }) => {
  const dispatch = useDispatch();

  document.body.style.overflowY = "hidden";
  const auth = useAuth();
  const history = useHistory();
  const [email, setEmail] = useState("malnomis99@googlemail.com");
  const [password, setPassword] = useState("Qwerty99");
  const [errorMsg, setErrorMsg] = useState("");
  const [regForename, setRegForename] = useState("Anony");
  const [regSurname, setRegSurname] = useState("mous");
  const [regDob, setRegDob] = useState(new Date(2001, 1, 1));
  const [regEmail, setRegEmail] = useState("");
  const [regPassword, setRegPassword] = useState("");
  const initialRender = useRef(true);
  let [mode, setMode] = useState(SignInOverlayModes.SIGN_IN_MODE);

  //TODO: onSuccess registering new user: setMode(SignInOverlayModes.REGISTER_MODE_SUCCESS);

  useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      if (thisAuthJwt) history.push("/explore");
    };
    getAuthInfo();
  }, []);

  const handleLogin = async (e) => {
    e.preventDefault();
    setErrorMsg("");
    if (!email || !password) {
      setErrorMsg("* Please enter valid email and password");
      return;
    }
    auth.login({ email, password });
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    setErrorMsg("");
    let errorMessage = "";

    let emailRegEx = validateEmail;
    let passwordRegEx = validatePassword;

    (!regEmail || !regPassword) &&
      errorMessage.concat("* Please enter all required fields. ");
    // @ts-ignore
    !regEmail.match(emailRegEx) &&
      errorMessage.concat("Please enter a valid email. ");
    // @ts-ignore
    !regPassword.match(passwordRegEx) &&
      errorMessage.concat("Password must meet minimum requirements.");

    if (errorMessage.length > 0) {
      setErrorMsg(errorMessage);
    } else {
      auth.register({
        firstName: regForename,
        lastName: regSurname,
        dob: dateToDdmmyyyyString(regDob),
        email: regEmail,
        pw: regPassword,
      });
    }
  };

  return (
    <div className="overlay-container">
      <div
        className="overlay"
        onClick={() => {
          toggleSignIn();
          setMode(SignInOverlayModes.SIGN_IN_MODE);
          setErrorMsg("");
        }}
      ></div>
      <div className="main-signin-container">
        {mode === SignInOverlayModes.REGISTER_MODE_SUCCESS ? (
          <>
            <div className="login-register-form">
              <img
                src={CheckCircle}
                alt="Registration Success"
                className="register-success-tick"
              ></img>
              <h1 className="form-title">Registration Successful!</h1>
              <p style={{ padding: "12px 0" }}>
                Please check your email and follow the activation link.
              </p>
              <Button
                text={"Return To Sign In"}
                styleOverrides={{
                  backgroundColor: "var(--button-blue)",
                  padding: "0.75rem",
                  marginTop: "28px",
                  fontSize: "0.85rem",
                }}
                onClickCallback={function () {
                  setMode(SignInOverlayModes.SIGN_IN_MODE);
                }}
              />
            </div>
          </>
        ) : mode === SignInOverlayModes.PASSWORD_RESET_MODE ? (
          <form className="login-register-form">
            <div className="form-title-back">
              <img
                src={ArrowLeft}
                alt="Go back"
                className="form-back-arrow"
                onClick={() => {
                  setMode(SignInOverlayModes.SIGN_IN_MODE);
                  setErrorMsg("");
                }}
              ></img>
              <h1 className="form-title">Reset Password</h1>
            </div>

            <p>
              Forgot your password? Enter your email and we'll send you a
              recovery link.
            </p>

            <div className="input-label">
              <label htmlFor="emailResetInput" className="input-label">
                Email
              </label>
              <input
                id="emailResetInput"
                className="form-input-text"
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                placeholder="Email"
                required
              />
            </div>
            <Button
              text={"Reset Password"}
              styleOverrides={{
                backgroundColor: "var(--button-blue)",
                padding: "0.75rem",
                marginTop: "28px",
                fontSize: "0.85rem",
              }}
              onClickCallback={handleLogin}
            />
          </form>
        ) : mode === SignInOverlayModes.REGISTER_MODE ? (
          <>
            <form
              className="login-register-form"
              onSubmit={handleRegister}
              autoComplete="off"
            >
              <div className="form-title-back">
                <img
                  src={ArrowLeft}
                  alt="Go back"
                  className="form-back-arrow"
                  onClick={() => setMode(SignInOverlayModes.SIGN_IN_MODE)}
                ></img>
                <h1 className="form-title">Create Account</h1>
              </div>
              <div
                className="error-message"
                style={{
                  visibility: errorMsg.length > 0 ? "visible" : "hidden",
                }}
              >
                {errorMsg}
              </div>
              <div className="input-label">
                <label htmlFor="emailResetInput" className="input-label">
                  Email
                </label>
                <input
                  id="emailResetInput"
                  className="form-input-text"
                  type="email"
                  value={regEmail}
                  onChange={(event) => setRegEmail(event.target.value)}
                  placeholder="Email"
                  required
                />
              </div>

              <div className="input-label">
                <label htmlFor="passwordInput" className="input-label">
                  Password
                </label>
                <input
                  id="passwordInput"
                  className="form-input-text"
                  type="password"
                  value={regPassword}
                  onChange={(event) => setRegPassword(event.target.value)}
                  placeholder="Password"
                  required
                />
              </div>
              <Button
                text={"Create Account"}
                styleOverrides={{
                  backgroundColor: "var(--button-blue)",
                  padding: "0.75rem",
                  marginTop: "28px",
                  fontSize: "0.85rem",
                }}
                onClickCallback={handleRegister}
              />
            </form>
          </>
        ) : mode === SignInOverlayModes.SIGN_IN_MODE ? (
          <>
            <h1 className="form-title">Sign In</h1>
            <form className="login-register-form">
              <div className="input-label">
                <span className="error-message">{errorMsg}</span>
                <label htmlFor="emailInput" className="input-label">
                  Email
                </label>
                <input
                  id="emailInput"
                  className="form-input-text"
                  type="email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                  placeholder="Email"
                  required
                />
              </div>

              <div className="input-label">
                <label htmlFor="passwordInput" className="input-label">
                  Password
                </label>
                <input
                  id="passwordInput"
                  className="form-input-text"
                  type="password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                  placeholder="Password"
                  required
                />
              </div>
              <span
                className="form-link"
                onClick={() => {
                  setMode(SignInOverlayModes.PASSWORD_RESET_MODE);
                }}
              >
                Forgotten password?
              </span>

              <Button
                text={"Sign In"}
                styleOverrides={{
                  backgroundColor: "var(--button-blue)",
                  padding: "0.75rem",
                  marginTop: "7px",
                  fontSize: "0.85rem",
                  ":hover": { backgroundColor: "red" },
                }}
                onClickCallback={(e) => handleLogin(e)}
              />

              <div className="create-new-account-link-container">
                <span
                  className="form-link"
                  onClick={() => {
                    setMode(SignInOverlayModes.REGISTER_MODE);
                    setErrorMsg("");
                  }}
                >
                  Create an account!
                </span>
              </div>
            </form>
          </>
        ) : (
          "How did you get here?"
        )}
      </div>
    </div>
  );
};
