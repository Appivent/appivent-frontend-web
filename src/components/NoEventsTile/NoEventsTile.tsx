import React from 'react';
import './NoEventsTile.css';

export function NoEventsTile({ text }) {

  return (
    <div className="no-events-tile-container">
      <span className="no-events-text">{text}</span>
    </div>
  );
}

export default NoEventsTile;
