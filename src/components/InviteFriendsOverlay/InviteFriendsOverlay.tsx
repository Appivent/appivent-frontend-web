import React from "react";
import { isEventInPast } from "../../helpers/utils";
import "./InviteFriendsOverlay.css";
import ProfileTile from "../ProfileTile/ProfileTile";
import Button from "../Button/Button";
import { useSearchFriends } from "../../hooks/user/use-user";
import useDebounce from "../../hooks/use-debounce";
import { useInviteUsers } from "../../hooks/event/use-event";

export function InviteFriendsOverlay({
  eventDetails,
  setShowInviteFriendsOverlay,
}) {
  const [foundFriends, setFoundFriends] = React.useState([] as any);
  const [searchFriendsQuery, setSearchFriendsQuery] = React.useState("");

  const { mutate: inviteUsersMutation } = useInviteUsers({
    lifecycleNotifications: {
      success: "Succesfully invited user/s",
      error: "Unable to invite user/s",
    },
  });

  const debouncedSearchQuery = useDebounce(searchFriendsQuery, 600);
  const { refetch: refetchSearchFriends } = useSearchFriends({
    lifecycleNotifications: { error: "Unable to search friends" },
    query: { query: debouncedSearchQuery },
    onSuccessCallback: (res) => {
      const attendingPendingFriends = eventDetails?.attending?.concat(
        ...eventDetails.pending
      );
      let friendsNotAttendingOrPending = [] as any;

      res?.forEach((searchedFriend) => {
        let exists = false;
        attendingPendingFriends?.forEach((attendingPendingFriend) => {
          if (attendingPendingFriend.userId === searchedFriend.userId) {
            exists = true;
          }
        });

        if (!exists) {
          friendsNotAttendingOrPending.push(searchedFriend);
        }
      });

      setFoundFriends(friendsNotAttendingOrPending);
    },
  });

  React.useEffect(() => {
    isEventInPast(eventDetails.startTimestamp);
  }, [eventDetails]);

  const searchForFriend = async (query) => {
    if (query.length > 3) {
      refetchSearchFriends();
    }
  };

  const inviteFriend = async (userId) => {
    inviteUsersMutation({ query: { eventId: eventDetails.eventId, userId } });
    setSearchFriendsQuery("");
  };

  return (
    <div className="overlay-container">
      <div
        className="overlay"
        onClick={() => {
          setShowInviteFriendsOverlay(false);
        }}
      ></div>
      <div className="invite-friends-overlay-container">
        <h3>Invite Friends</h3>
        <div className="invite-friends-container">
          <input
            className="invite-friends-input"
            placeholder="Search..."
            value={searchFriendsQuery}
            onChange={(e) => {
              setSearchFriendsQuery(e.target.value);
              if (e.target.value.length > 3) {
                searchForFriend(e.target.value);
              }
            }}
          />
        </div>
        {searchFriendsQuery.length > 3 &&
          foundFriends.map((friend, i) => {
            return (
              <div className="invite-friend-container">
                <ProfileTile key={i} user={friend} onXClicked={null} />
                <Button
                  text={"Invite"}
                  styleOverrides={{
                    backgroundColor: "var(--light-orange)",
                    marginLeft: "5px",
                    maxWidth: "200px",
                  }}
                  onClickCallback={function () {
                    inviteFriend(friend.userId);
                  }}
                ></Button>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default InviteFriendsOverlay;
