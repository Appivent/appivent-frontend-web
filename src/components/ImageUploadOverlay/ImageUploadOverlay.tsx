import "./ImageUploadOverlay.css";
import { useState, useCallback, useRef, useEffect } from "react";
import { useDispatch } from "react-redux";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import Button from "../../components/Button/Button";

const ImageUploadOverlay = ({ updateProfilePic, toggleImgUploadOverlay }) => {
  const dispatch = useDispatch();
  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({ unit: "%", width: 30, aspect: 1 / 1 });
  const [completedCrop, setCompletedCrop] = useState(null);

  const [toggleUploadCancel, setToggleUploadCancel] = useState(false);

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      // @ts-ignore
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
      setToggleUploadCancel(!toggleUploadCancel);
    }
  };

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  function generateDownload(canvas, crop) {
    if (!crop || !canvas) {
      return;
    }
    const reader = new FileReader();
    if (canvas) {
      const base64Canvas = canvas.toDataURL("image/jpeg").split(";base64,")[1];
      updateProfilePic({ query: {base64ImageString: base64Canvas }});
    }
  }

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }
    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const crop = completedCrop;

    // @ts-ignore
    const scaleX = image.naturalWidth / image.width;
    // @ts-ignore
    const scaleY = image.naturalHeight / image.height;
    // @ts-ignore
    const ctx = canvas.getContext("2d");
    const pixelRatio = window.devicePixelRatio;
    // @ts-ignore
    canvas.width = crop.width * pixelRatio;
    // @ts-ignore
    canvas.height = crop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = "high";

    // @ts-ignore
    ctx.drawImage(image,crop.x * scaleX,crop.y * scaleY,crop.width * scaleX,crop.height * scaleY,0,0,crop.width,crop.height);
  }, [completedCrop]);

  return (
    <div className="global-middle-section">
      <div className="overlay-container">
        <div
          className="overlay"
          onClick={() => {
            toggleImgUploadOverlay(false);
          }}
        ></div>
        <div className="image-upload-overlay-container">
          <input type="file" accept="image/*" onChange={onSelectFile} />
          <ReactCrop
            src={upImg}
            onImageLoaded={onLoad}
            crop={crop}
            onChange={(c) => setCrop(c)}
            onComplete={(c) => setCompletedCrop(c)}
          />
          <canvas
            ref={previewCanvasRef}
            // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
            style={{
              display: "none",
            }}
          />

          <div className="image-upload-overlay-buttons-container">
            {toggleUploadCancel && (
              <Button
                text="Upload"
                styleOverrides={{
                  backgroundColor: "var(--facebook-blue)",
                  margin: "0 5px",
                }}
                onClickCallback={() =>
                  generateDownload(previewCanvasRef.current, completedCrop)
                }
              />
            )}
            <Button
              text="Cancel"
              styleOverrides={{
                backgroundColor: "var(--dark-orange)",
                margin: "0 5px",
              }}
              onClickCallback={() => toggleImgUploadOverlay()}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImageUploadOverlay;
