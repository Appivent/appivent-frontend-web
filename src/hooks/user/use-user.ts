import { useMutation, UseQueryResult } from "react-query";
import { useDispatch } from "react-redux";
import { useQuery } from "react-query";
import { UserStateKeysEnum } from "./keys";
import { callbackBuilder } from "../callbacks-builder";
import { DEFAULT_API_CONFIG as config } from "../../services/api-config";
import { useAuth } from "../use-auth";

export const useGetUser = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const authJwt = useAuth().accessToken;

  return useQuery(
    UserStateKeysEnum.GetUser + query.userId,
    () =>
      fetch(config.USERS_LAMBDA_URL + query.userId, {
        headers: new Headers({
          Authorization: authJwt,
        }),
      }),
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

export const useGetNotifications = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const authJwt = useAuth().accessToken;

  return useQuery(
    UserStateKeysEnum.GetNotifications,
    async () => {
      const res = await fetch(config.USERS_LAMBDA_URL + "notifications", {
        headers: new Headers({
          Authorization: authJwt,
        }),
      });
      return await res.json();
    },
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

export const useGetFriendsList = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const dispatch = useDispatch();
  const authJwt = useAuth().accessToken;

  return useQuery(
    UserStateKeysEnum.GetUser + query.userId,
    async () => {
      const res = await fetch(
        config.USERS_LAMBDA_URL + "friends?offset=0&limit=100",
        {
          headers: new Headers({
            Authorization: authJwt,
          }),
        }
      );
      return res.json();
    },
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

export const useSearchUsers = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const dispatch = useDispatch();
  const authJwt = useAuth().accessToken;

  return useQuery(
    UserStateKeysEnum.GetUser + query.userId,
    async () => {
      const res = await fetch(
        config.USERS_LAMBDA_URL + "users/" + query.query,
        {
          headers: new Headers({
            Authorization: authJwt,
          }),
        }
      );
      return res.json();
    },
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

export const useSearchFriends = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const dispatch = useDispatch();
  const authJwt = useAuth().accessToken;

  return useQuery(
    UserStateKeysEnum.GetUser + query.userId,
    async () => {
      const res = await fetch(
        config.USERS_LAMBDA_URL + "connections/" + query.query,
        {
          headers: new Headers({
            Authorization: authJwt,
          }),
        }
      );
      return await res.json();
    },
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

export const useAddFriend = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.USERS_LAMBDA_URL + "request/" + query.requestUserId, {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useUpdateUserDetails = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.USERS_LAMBDA_URL + query.userId, {
      method: "put",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: JSON.stringify(query.userDetails),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useRespondFriendRequest = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(
      config.USERS_LAMBDA_URL +
        "connection/" +
        query.userId +
        "/" +
        query.isAccepted,
      {
        method: "put",
        headers: new Headers({
          Authorization: authJwt,
        }),
        body: JSON.stringify(query.userDetails),
      }
    );
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useRemoveFriend = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.USERS_LAMBDA_URL + "remove/" + query.userId, {
      method: "delete",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: JSON.stringify(query.userDetails),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useUpdateProfilePic = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.IMAGE_LAMBDA_URL + "user/image/", {
      method: "put",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: query.base64ImageString,
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useRemoveProfilePic = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.IMAGE_LAMBDA_URL + "user/image", {
      method: "delete",
      headers: new Headers({
        Authorization: authJwt,
      }),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useDeleteAccount = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.USERS_LAMBDA_URL + "delete", {
      method: "delete",
      headers: new Headers({
        Authorization: authJwt,
      }),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};
