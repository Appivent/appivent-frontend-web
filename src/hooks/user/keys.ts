export enum UserStateKeysEnum {
  GetUser = "getUser",
  GetNotifications = "getNotifications",
}
