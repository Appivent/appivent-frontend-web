export const callbackBuilder = (
  onSuccessCallback,
  onErrorCallback,
  lifecycleNotifications,
  autoFetch = false,
) => {
  return {
    enabled: autoFetch,
    onSuccess: (res) => {
      onSuccessCallback && onSuccessCallback(res);
      if (lifecycleNotifications.success) {
        //TODO: dispatch success notification
      }
    },
    onError: (err) => {
      onErrorCallback && onErrorCallback();
      if (lifecycleNotifications.error) {
        //TODO: dispatch error notification
      }
    },
  };
};
