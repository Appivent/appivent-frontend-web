import { useState, useContext, createContext } from "react";
import Amplify, { Auth } from "aws-amplify";

Amplify.configure({
  Auth: Amplify.configure(
    JSON.parse(process.env.REACT_APP_AMPLIFY_CONFIG || "")
  ),
});

const authContext = createContext({} as any);

export function ProvideAuth({ children }) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
  return useContext(authContext);
};

function useProvideAuth() {
  const [user, setUser] = useState(null as any);
  const [userId, setUserId] = useState(null as any);
  const [accessToken, setAccessToken] = useState(null as any);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const login = async ({ email, password }) => {
    try {
      const res = await Auth.signIn(email, password);
      if (res.message) {
        throw new Error(res.message);
      } else {
        const res = await getCurrentSession();
        setAccessToken(res.accessToken);
        setUserId(res.userId);
        setUser(Auth.currentAuthenticatedUser());
        setIsLoggedIn(true);

        //TODO: Do we need refresh tokens? The below can be used to setInterval() and get a new one
        // try {
        //   const cognitoUser = await Auth.currentAuthenticatedUser();
        //   const currentSession = await Auth.currentSession();
        //   cognitoUser.refreshSession(currentSession.refreshToken, (err, session) => {
        //     console.log('session', err, session);
        //     const { idToken, refreshToken, accessToken } = session;
        //     // do whatever you want to do now :)
        //   });
        // } catch (e) {
        //   console.log('Unable to refresh Token', e);
        // }
      }
    } catch (error) {
      return error;
    }
  };

  const register = async (forename, surname, dob, email, password) => {
    try {
      const res = await Auth.signUp({
        username: email,
        password,
        attributes: {
          birthdate: dob,
          family_name: surname,
          name: forename,
        },
      });

      // if (res) {
      //   throw new Error("Cannot sign up");
      // } else {
      return true;
      // }
    } catch (error: any) {
      if (error.code === "UsernameExistsException") {
        try {
          return Auth.resendSignUp(email);
        } catch (err) {
          return err;
        }
      } else {
        return error;
      }
    }
  };

  const resetPassword = (email, code, newPassword) => {
    return Auth.forgotPasswordSubmit(email, code, newPassword)
      .then((data) => data)
      .catch((err) => err);
  };

  const forgotPassword = (email) => {
    return Auth.forgotPassword(email)
      .then((data) => data)
      .catch((err) => err);
  };

  const logout = () => {
    try {
      return Auth.signOut();
    } catch (error) {
      return error;
    }
  };

  const changePassword = (oldPassword, newPassword) => {
    return Auth.currentAuthenticatedUser()
      .then((user) => {
        return Auth.changePassword(user, oldPassword, newPassword);
      })
      .then((data) => data)
      .catch((err) => err);
  };

  const changeEmail = (newEmail) => {
    let user = Auth.currentAuthenticatedUser();

    let res = Auth.updateUserAttributes(user, {
      email: newEmail,
    });
    return res;
  };

  const getCurrentSession = async () => {
    let userId = "";
    let accessToken = "";

    try {
      const currentSession = await Auth.currentSession()
        .then((data) => data)
        .catch((err) => {
          console.log(err);
          return err;
        });

      userId = currentSession.getIdToken().payload.sub;
      accessToken = currentSession.getIdToken().getJwtToken();
    } catch (e) {
      // Do nothing
    }

    return {
      userId,
      accessToken,
    };
  };

  return {
    user,
    isLoggedIn,
    accessToken,
    login,
    logout,
    register,
    resetPassword,
    forgotPassword,
    changePassword,
    changeEmail,
    getCurrentSession,
  };
}
