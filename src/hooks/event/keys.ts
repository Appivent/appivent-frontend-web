export enum EventStateKeysEnum {
  GetEvents = "getEvents",
  GetEvent = "getEvent",
  GetLocations = "getLocations"
}
