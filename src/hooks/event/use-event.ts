import { useMutation, UseQueryResult } from "react-query";
import { useQuery } from "react-query";
import { EventStateKeysEnum } from "./keys";
import { callbackBuilder } from "../callbacks-builder";
import { DEFAULT_API_CONFIG as config } from "../../services/api-config";
import { useAuth } from "../use-auth";

//The below accommodates for user specific events
export const useGetEventsAndFilters = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const authJwt = useAuth().accessToken;
  const isUserSpecific = query.userId ? `/${query.userId}` : "";

  return useQuery(
    EventStateKeysEnum.GetEvents,
    async () => {
      let requestUrl =
        config.EVENTS_LAMBDA_URL +
        "events" +
        isUserSpecific +
        "?limit=" +
        query.limit +
        "&offset=" +
        query.offset +
        "&sortOption=" +
        query.selectedSortOption;
      if (query.locationFilter)
        requestUrl += "&locationFilter=" + query.locationFilter;

      if (query.tags.length > 0) requestUrl += "&tags=" + query.tags.join(",");

      if (query.fromDate) requestUrl += "&fromDate=" + query.fromDate;
      if (query.toDate) requestUrl += "&toDate=" + query.toDate;

      let res = await fetch(requestUrl, {
        headers: new Headers({
          Authorization: authJwt,
        }),
      });

      return res.json();
    },
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

//TODO: invalidate this query key when updated event, or attending/leaving event
export const useGetEvent = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const authJwt = useAuth().accessToken;
  const shouldUseRedacted = authJwt ? "event/" : "event-redacted/";

  return useQuery(
    EventStateKeysEnum.GetEvent,
    async () => {
      const res = await fetch(
        config.EVENTS_LAMBDA_URL + shouldUseRedacted + query.eventId,
        {
          headers: new Headers({
            Authorization: authJwt,
          }),
        }
      );
      return await res.json();
    },
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

export const useGetLocations = ({
  autoFetch,
  query,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any): UseQueryResult<any, Error> => {
  const authJwt = useAuth().accessToken;

  return useQuery(
    EventStateKeysEnum.GetLocations,
    async () => {
      const res = await fetch(config.LOCATION_LAMBDA_URL + "location/" + query.query, {
        headers: new Headers({
          Authorization: authJwt,
        }),
      });
      return await res.json();
    },
    callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch)
  );
};

export const useCreateEvent = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.EVENTS_LAMBDA_URL + "event", {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: JSON.stringify(query.eventData),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useUpdateEvent = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(
      config.EVENTS_LAMBDA_URL + "event/" + query.eventData.eventId,
      {
        method: "put",
        headers: new Headers({
          Authorization: authJwt,
        }),
        body: JSON.stringify(query.eventData),
      }
    );
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useDeleteEvent = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.EVENTS_LAMBDA_URL + "event/" + query.eventId, {
      method: "delete",
      headers: new Headers({
        Authorization: authJwt,
      }),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useUpdateEventPic = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.IMAGE_LAMBDA_URL + "event/image/" + query.eventId, {
      method: "put",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: JSON.stringify(query.base64ImageString),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useDeleteEventPic = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    return fetch(config.IMAGE_LAMBDA_URL + "event/image/" + query.eventId, {
      method: "delete",
      headers: new Headers({
        Authorization: authJwt,
      }),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useInviteUsers = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    //The below body can take an array of invited userId, so maybe in future, re-design this?
    return fetch(config.EVENTS_LAMBDA_URL + "event/invite/" + query.eventId, {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: JSON.stringify({ invited: [query.userId] }),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useRsvpEvent = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    //The below body can take an array of invited userId, so maybe in future, re-design this?
    return fetch(
      config.EVENTS_LAMBDA_URL +
        "event/rsvp/" +
        query.eventId +
        "/" +
        query.isAttending,
      {
        method: "put",
        headers: new Headers({
          Authorization: authJwt,
        }),
      }
    );
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useJoinEvent = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    //The below body can take an array of invited userId, so maybe in future, re-design this?
    return fetch(config.EVENTS_LAMBDA_URL + "event/join/" + query.eventId, {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};

export const useLeaveEvent = ({
  autoFetch,
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback,
}: any) => {
  const authJwt = useAuth().accessToken;

  return useMutation(({ query }: any) => {
    //The below body can take an array of invited userId, so maybe in future, re-design this?
    return fetch(
      config.EVENTS_LAMBDA_URL +
        "event/" +
        query.eventId +
        "/remove/" +
        query.userId,
      {
        method: "delete",
        headers: new Headers({
          Authorization: authJwt,
        }),
      }
    );
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications, autoFetch));
};
