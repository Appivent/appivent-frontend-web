import { useMutation, UseQueryResult } from "react-query";
import { useDispatch } from "react-redux";
import { useQuery } from "react-query";
import { InteractionStateKeysEnum } from "./keys";
import { callbackBuilder } from "../callbacks-builder";
import { DEFAULT_API_CONFIG as config } from "../../services/api-config";

//TODO: The below needs to be finalized

export const usePostComment = (
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback
): any => {
  const authJwt = ""; //TODO: Get JWT;

  return useMutation(({ query }: any) => {
    return fetch(config.EVENTS_LAMBDA_URL + "event", {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: query.comment,
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications));
};

export const useEditComment = (
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback
): any => {
  const authJwt = ""; //TODO: Get JWT;

  return useMutation(({ query }: any) => {
    return fetch(config.EVENTS_LAMBDA_URL + "event", {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: query.comment,
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications));
};

export const useDeleteComment = (
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback
): any => {
  const authJwt = ""; //TODO: Get JWT;

  return useMutation(({ query }: any) => {
    return fetch(config.EVENTS_LAMBDA_URL + "event", {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: query.comment,
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications));
};

export const useLikeComment = (
  lifecycleNotifications,
  onSuccessCallback,
  onErrorCallback
): any => {
  const authJwt = ""; //TODO: Get JWT;

  return useMutation(({ query }: any) => {
    return fetch(config.EVENTS_LAMBDA_URL + "event", {
      method: "post",
      headers: new Headers({
        Authorization: authJwt,
      }),
      body: query.comment,
    });
  }, callbackBuilder(onSuccessCallback, onErrorCallback, lifecycleNotifications));
};