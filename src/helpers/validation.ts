// The below properties are configured on backend too
export const MAX_EVENT_DESCRIPTION_LENGTH = 2000;
export const MAX_TITLE_LENGTH = 120;
export const MAX_TAG_LENGTH = 15;

export function validateAlphaNumeric(query: string){
  const alphaNumericRegex = RegExp('[a-zA-Z0-9]', 'g');
  return alphaNumericRegex.test(query)
}

export function validateLetters(query: string){
  const lettersRegex = RegExp('[a-zA-Z]', 'g');
  return lettersRegex.test(query)
}

export function validateDate(query: string){
  const dateRegex = /^(\d+-?)+\d+$/i
  return dateRegex.test(query)
}

export function validateEmail(query: string){
  const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  return emailRegex.test(query)
}

export function validatePassword(query: string){
  const validateRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/i;
  return validateRegex.test(query)
}