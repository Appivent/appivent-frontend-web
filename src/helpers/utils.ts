export const dateToUnixTimestampToString = (date: Date) => {
    return date ? Math.round(date.getTime() / 1000).toString() : "";
}

export const unixTimestampStringToDate = (unixTimestampString: string) => {
    return unixTimestampString ? new Date(parseInt(unixTimestampString) * 1000) : null;
}

export const minimumAgeDate = () => new Date().setFullYear( new Date().getFullYear()-13);

export const dateToDdmmyyyyString = (date) => {
    if (!date) return "";
    
    let dd = date.getDate();
    let mm = date.getMonth()+1; 
    let yyyy = date.getFullYear();
    if (dd < 10)
        //@ts-ignore
        dd='0'+dd;
    
    if (mm < 10)
        //@ts-ignore
        mm='0'+mm;

    return dd+'/'+mm+'/'+yyyy;
}

export const dateStringDdmmyyyToMmddyyyString = (slashDateString) => {
    const dateStringSplit = slashDateString.split("/");
    return dateStringSplit[1] + "/" + dateStringSplit[0] + "/" + dateStringSplit[2]
}

export const isSameDate = (firstDate, secondDate) => {
    const isSameDate = firstDate.getDate() === secondDate.getDate();
    const isSameMonth = firstDate.getMonth() === secondDate.getMonth();
    const isSameYear = firstDate.getYear() === secondDate.getYear();

    if (isSameDate && isSameMonth && isSameYear) {
      return true;
    }
    return false;
}

export const unixTimestampToReadableDateAndTime = (timestamp) => {
  const date = new Date(timestamp * 1000)
  const readableDate = date.toDateString();
  const minutes = date.getMinutes().toString();
  const formattedMinutes = minutes.length === 1 ? minutes + "0" : minutes
  const readableTime = date.getHours() + ":" + formattedMinutes;
  return readableDate + " " + readableTime;
}

export const unixTimestampToReadableDate = (timestamp) => {
  const date = new Date(timestamp * 1000)
  const readableDate = date.toDateString();
  return readableDate;
}

export const calculateDaysLeft = (startTimestamp, eventIsPast) => {
  if (eventIsPast) return "Finished";
  const today = new Date();
  const eventStartDate = new Date(startTimestamp * 1000);
  const differenceInTime = eventStartDate.getTime() - today.getTime();
  const differenceInDays = Math.floor(differenceInTime / (1000 * 3600 * 24)) + 1;
  const differenceInWeeks = Math.floor(differenceInDays / 7);
  const differenceInMonths = Math.floor(differenceInDays / 30.42); //average days in a month
  const differenceInYears = Math.floor(differenceInDays / 365);

  if (differenceInYears > 0) {
    return differenceInYears + (differenceInYears < 2 ? " year left" : " years left");
  } else if (differenceInMonths > 0) {
    return differenceInMonths + (differenceInMonths < 2 ? " month left" : " months left");
  } else if (differenceInWeeks > 0) {
    return differenceInWeeks + (differenceInWeeks < 2 ? " week left" : " weeks left");
  }
  return differenceInDays + (differenceInDays < 2 ? " day left" : " days left");
}

export const isEventInPast = (startTimestamp) => {
  //@ts-ignore
  return unixTimestampStringToDate(startTimestamp) < new Date();
}