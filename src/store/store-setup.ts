import { configureStore } from '@reduxjs/toolkit'
import appReducer from './app-slice';
import "regenerator-runtime/runtime";


export default configureStore({
  reducer: {
    app: appReducer,
  },
  middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware(),
});