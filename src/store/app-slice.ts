import { createSlice } from "@reduxjs/toolkit";

const eventsSlice = createSlice({
  name: "eventsReducer",
  initialState: {
    something: false,
  },
  reducers: {
    setSomething(state) {
      state.something = true;
    },
  },
});

const { actions, reducer } = eventsSlice;

export const { setSomething } = actions;

export default reducer;
