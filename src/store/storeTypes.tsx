export enum SignInOverlayModes {
    SIGN_IN_MODE = "signIn",
    REGISTER_MODE = "register",
    REGISTER_MODE_SUCCESS = "registerSuccess",
    PASSWORD_RESET_MODE = "passwordReset",
    PASSWORD_RESET_MODE_SUCCESS = "passwordResetSuccess",
}