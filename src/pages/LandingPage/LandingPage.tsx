import React from "react";
import { useHistory } from "react-router-dom";
import "./LandingPage.css";
import Button from "../../components/Button/Button";
import "react-datepicker/dist/react-datepicker.css";
import bannerPlaceholder from "../../assets/banner-placeholder.png";
import { dateToUnixTimestampToString } from "../../helpers/utils";
import { useGetEventsAndFilters } from "../../hooks/event/use-event";
import { useAuth } from "../../hooks/use-auth";

export const LandingPage = ({ toggleSignIn }) => {
  const auth = useAuth();
  const history = useHistory();

  const { data: eventsData, refetch: refetchGetEvents } =
    useGetEventsAndFilters({
      lifecycleNotifications: { error: "Unable to get events" },
      query: {
        offset: 0,
        limit: 10,
        selectedSortOption: "mpdesc",
        locationFilter: "",
        tags: [],
        fromDate: dateToUnixTimestampToString(new Date()),
        toDate: "",
      },
    });

  React.useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      if (thisAuthJwt) history.push("/home");
    };
    getAuthInfo();
  }, []);

  React.useEffect(() => {
    if (auth.isLoggedIn) {
      history.push("/home");
    }
  }, [auth.isLoggedIn]);

  const goToEvent = (eventId) => {
    history.push({
      pathname: "/event",
      search: "?eventId=" + eventId + "&mode=view",
    });
  };

  return (
    <>
      <div className="landing-page-container">
        <div className="cover-container">
          <div className="title-subtitle">
            <h1 className="cover-title">Create & Share Your Group Events</h1>
            <p className="cover-subtitle">
              Appivent helps you organize events and track invitees with a
              simple, no fuss platform
            </p>
          </div>

          <div className="banner-img">
            <img src={bannerPlaceholder} alt="Banner"></img>
          </div>
          <Button
            text={"Get Started"}
            onClickCallback={toggleSignIn}
            styleOverrides={{
              fontFamily: "'roboto', sans-serif",
              margin: "24px",
              padding: "0.7rem 2rem",
              color: "var(--white)",
              fontSize: "clamp(1.25rem, 3vmin, 1.4rem)",
              background: "var(--black)",
            }}
          ></Button>
        </div>
        {eventsData?.events?.length >= 10 && (
          <>
            <h1 className="explore-popular-events">Explore Popular Events</h1>
            <div className="popular-events-container">
              {eventsData?.events?.length > 0 &&
                eventsData?.events?.map((event, index) => {
                  return (
                    <div
                      key={index}
                      className="landing-page-event-tile-container"
                      style={{
                        backgroundImage: 'url("' + event.eventPicUrl + '")',
                      }}
                      onClick={() => {
                        goToEvent(event.eventId);
                      }}
                    >
                      <span className="landing-page-event-tile-title">
                        {event.title}
                      </span>
                    </div>
                  );
                })}
            </div>
          </>
        )}
      </div>
    </>
  );
};
