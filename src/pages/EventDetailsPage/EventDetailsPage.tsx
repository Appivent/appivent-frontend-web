import React from "react";
import { useHistory } from "react-router-dom";
import "./EventDetailsPage.css";
import { useSelector, useDispatch } from "react-redux";
import { useQuery } from "../../helpers/router-helper";
import Button from "../../components/Button/Button";
import "react-datepicker/dist/react-datepicker.css";
import ProfileTile from "../../components/ProfileTile/ProfileTile";
import LoadingOverlay from "../../components/LoadingOverlay/LoadingOverlay";
import EventDetailsPanel from "../../components/EventDetailsPanel/EventDetailsPanel";
import EventDetailsLocationPanel from "../../components/EventDetailsLocationPanel/EventDetailsLocationPanel";
import InviteFriendsOverlay from "../../components/InviteFriendsOverlay/InviteFriendsOverlay";
import AttendUnattendOverlay from "../../components/AttendUnattendOverlay/AttendUnattendOverlay";
import EventDetailsCommentsPanel from "../../components/EventDetailsCommentsPanel/EventDetailsCommentsPanel";
import { useGetEvent, useLeaveEvent } from "../../hooks/event/use-event";
import { useAuth } from "../../hooks/use-auth";

const MODES = {
  view: "view",
  edit: "edit",
};

export const EventDetailsPage = () => {
  const showLoadingEventOverlay = useSelector(
    (state) => state.events.eventDetailsLoading
  );

  const auth = useAuth();
  const history = useHistory();
  const query = useQuery();
  const eventId = query.get("eventId");
  const [authUserId, setAuthUserId] = React.useState("");
  const [authJwt, setAuthJwt] = React.useState("");

  const { data: eventDetails, refetch: refetchGetEvent } = useGetEvent({
    lifecycleNotifications: { error: "Unable to get event details" },
    query: { eventId },
  });

  const { mutate: leaveEventMutation } = useLeaveEvent({
    lifecycleNotifications: {
      success: "No longer attending",
      error: "Unable to leave",
    },
  });

  const [showInviteFriendsOverlay, setShowInviteFriendsOverlay] =
    React.useState(false);

  const [showAttendUnattendOverlay, setShowAttendUnattendOverlay] =
    React.useState(false);

  const isAdminOfEvent = eventDetails.adminId === authUserId;

  React.useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      const thisAuthUserId = auth.userId;

      setAuthJwt(thisAuthJwt);
      setAuthUserId(thisAuthUserId);
    };
    getAuthInfo();
  }, []);

  const isAttending = () => {
    let attending = false;
    if (eventDetails?.attending?.length === 0) {
      return false;
    }
    eventDetails?.attending?.map((user) => {
      if (user.userId === authUserId) {
        attending = true;
      }
    });
    return attending;
  };

  const goToPageWithModeAndEventId = (mode, eventId = eventDetails.eventId) => {
    history.push({
      pathname: "/update-event",
      search: "?eventId=" + eventId + "&mode=" + mode,
    });
    history.go(0);
  };

  const removeUserFromEvent = async (userId) => {
    leaveEventMutation({ query: { eventId, userId } });
  };

  return (
    <div className="global-middle-section">
      {showInviteFriendsOverlay && (
        <InviteFriendsOverlay
          eventDetails={eventDetails}
          setShowInviteFriendsOverlay={setShowInviteFriendsOverlay}
        />
      )}
      {showAttendUnattendOverlay && (
        <AttendUnattendOverlay
          authUserId={authUserId}
          eventDetails={eventDetails}
          isAttendingEvent={() => isAttending()}
          setShowAttendUnattendOverlay={setShowAttendUnattendOverlay}
        />
      )}
      <div className="event-details-container">
        <div className="event-buttons-wrapper">
          {isAdminOfEvent && (
            <Button
              text={"Edit"}
              styleOverrides={{
                backgroundColor: "var(--dark-blue)",
                margin: "0 5px",
              }}
              onClickCallback={function () {
                goToPageWithModeAndEventId(MODES.edit);
              }}
            ></Button>
          )}
          {eventDetails.attending && isAttending() && (
            <Button
              text={"Unattend"}
              styleOverrides={{
                backgroundColor: "var(--dark-orange)",
                margin: "0 5px",
              }}
              onClickCallback={function () {
                setShowAttendUnattendOverlay(true);
              }}
            ></Button>
          )}
          {eventDetails.attending && !isAttending() && (
            <Button
              text={"Attend"}
              styleOverrides={{
                backgroundColor: "var(--light-orange)",
                margin: "0 5px",
              }}
              onClickCallback={function () {
                setShowAttendUnattendOverlay(true);
              }}
            ></Button>
          )}
          {(isAttending() && eventDetails.allowGuestInvites) ||
          eventDetails.adminId === authUserId ? (
            <Button
              text={"Invite"}
              styleOverrides={{
                backgroundColor: "var(--light-blue)",
                margin: "0 5px",
                maxWidth: "200px",
              }}
              onClickCallback={function () {
                setShowInviteFriendsOverlay(true);
              }}
            ></Button>
          ) : null}
        </div>
        <div className="event-details-left-right">
          {showLoadingEventOverlay ? <LoadingOverlay text={""} /> : null}
          <div className="event-details-left">
            <EventDetailsPanel eventDetails={eventDetails} />
            <EventDetailsLocationPanel eventDetails={eventDetails} />
            <EventDetailsCommentsPanel eventDetails={eventDetails} />
          </div>
          <div className="event-details-right">
            <div className="event-guest-list-wrapper">
              <h3>Guest List</h3>
              {
                <div className="invited-container">
                  {!authJwt && (
                    <span className="sign-in-to-view">Sign in to view</span>
                  )}
                  {eventDetails?.attending?.map((attendingProfile, index) => (
                    <ProfileTile
                      onXClicked={isAdminOfEvent ? removeUserFromEvent : null}
                      key={index}
                      user={attendingProfile}
                      containerStyleOverrides={{ maxWidth: "200px" }}
                      imageStyleOverrides={{
                        border: "none",
                        boxShadow: "0 0 0 3px var(--green)",
                        outline: "none",
                      }}
                    />
                  ))}
                  {eventDetails?.pending?.map((pendingProfile, index) => (
                    <ProfileTile
                      onXClicked={isAdminOfEvent ? removeUserFromEvent : null}
                      key={index}
                      user={pendingProfile}
                      imageStyleOverrides={{
                        border: "none",
                        boxShadow: "0 0 0 3px var(--orange)",
                        outline: "none",
                      }}
                    />
                  ))}
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
