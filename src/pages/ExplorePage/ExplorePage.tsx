import React from "react";
import { useHistory, useLocation } from "react-router-dom";
import "./ExplorePage.css";
import { useDispatch } from "react-redux";
import { useQuery } from "../../helpers/router-helper";
import FilterPanel from "../../components/FilterPanel/FilterPanel";
import Button from "../../components/Button/Button";
import EventTile from "../../components/EventTile/EventTile";
import NoEventsTile from "../../components/NoEventsTile/NoEventsTile";
import {
  dateToUnixTimestampToString,
  unixTimestampStringToDate,
} from "../../helpers/utils";
import InfiniteScroll from "react-infinite-scroll-component";
import { useGetEventsAndFilters } from "../../hooks/event/use-event";
import { useAuth } from "../../hooks/use-auth";

export const ExplorePage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const query = useQuery();
  const auth = useAuth();

  const limit = 10;
  const userId = query.get("userId");
  const [events, setEvents] = React.useState([] as any);
  const [selectedSortOption, setSelectedSortOption] = React.useState("");
  const [locationFilter, setLocationFilter] = React.useState("");
  const [tags, setTags] = React.useState([]);
  const [totalEvents, setTotalEvents] = React.useState(0);
  const [fromDate, setFromDate] = React.useState(new Date());
  const [toDate, fromToDate] = React.useState(new Date());
  const [sortOptions, setSortOptions] = React.useState([] as any);
  const [authUserId, setAuthUserId] = React.useState("");
  const [offset, setOffset] = React.useState(0);

  const [selectedSortOptionQuery, setSelectedSortOptionQuery] =
    React.useState(selectedSortOption);
  const [locationFilterQuery, setLocationFilterQuery] =
    React.useState(locationFilter);
  const [tagsFilterQuery, setTagsFilterQuery] = React.useState(tags);
  const [fromDateFilterQuery, setFromDateFilterQuery] = React.useState(
    new Date()
  );
  const [toDateFilterQuery, setToDateFilterQuery] = React.useState(null);

  const [updatedFromFilterPanel, setUpdatedFromFilterPanel] =
    React.useState(true);

  const { refetch: refetchGetEvents } = useGetEventsAndFilters({
    lifecycleNotifications: { error: "Unable to get events" },
    query: {
      offset,
      limit,
      selectedSortOption,
      locationFilter,
      tags,
      fromDate: dateToUnixTimestampToString(fromDate),
      toDate: dateToUnixTimestampToString(toDate),
      userId,
    },
  });

  React.useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthUserId = auth.userId;
      setAuthUserId(thisAuthUserId);
      //Might not need the below
      //refetchGetEvents();
    };
    getAuthInfo();
  }, []);

  React.useEffect(() => {
    if (!updatedFromFilterPanel) {
      setLocationFilterQuery(locationFilter);
      setSelectedSortOptionQuery(selectedSortOption);
      setTagsFilterQuery(tags);
      // @ts-ignore
      setFromDateFilterQuery(unixTimestampStringToDate(fromDate));
      // @ts-ignore
      setToDateFilterQuery(unixTimestampStringToDate(toDate));
      setUpdatedFromFilterPanel(false);
    }
  }, [events]); // eslint-disable-line react-hooks/exhaustive-deps

  const areAllEventsLoaded = () => {
    return totalEvents > offset + 10;
  };

  const createEventHandler = () => {
    history.push({
      pathname: "/update-event",
      search: "?mode=create",
    });
  };

  const paginate = () => {
    setOffset(offset + limit);
    refetchGetEvents();
  };

  const goToEvent = (eventId) => {
    history.push({
      pathname: "/event",
      search: "?eventId=" + eventId + "&mode=view",
    });
  };

  const clearFilters = () => {
    setLocationFilterQuery("");
    setTagsFilterQuery([]);
    setFromDateFilterQuery(new Date());
    setToDateFilterQuery(null);
    refetchGetEvents();
    //TODO: Reset filters states here
    // dispatch(
    //   getEventsFromApi({
    //     offset: 0,
    //     limit,
    //     selectedSortOption: selectedSortOptionQuery,
    //     location: "",
    //     tags: [],
    //     fromDate: dateToUnixTimestampToString(new Date()),
    //     toDate: "",
    //     userId,
    //   })
    // );
  };

  return (
    <>
      <FilterPanel
        setLocationFilterQuery={setLocationFilterQuery}
        setTagsFilterQuery={setTagsFilterQuery}
        setFromDateFilterQuery={setFromDateFilterQuery}
        setToDateFilterQuery={setToDateFilterQuery}
        tagsFilterQuery={tagsFilterQuery}
        fromDateFilterQuery={fromDateFilterQuery}
        toDateFilterQuery={toDateFilterQuery}
        totalEvents={totalEvents}
        clearFilters={clearFilters}
      />
      <div className="events-container">
        <div className="events-list-container">
          <div className="events-list-header">
            <div className="events-list-header-left">
              <h3 style={{ marginRight: "10px" }}>Explore New Events</h3>
              <Button
                text="Create Event"
                styleOverrides={{
                  backgroundColor: "var(--dark-blue)",
                  verticalAlign: "middle",
                }}
                onClickCallback={createEventHandler}
              ></Button>
            </div>
            <div className="filter-type-container">
              <span className="filter-panel-text">Sort by:</span>
              <select
                className="filter-select-box sort-by-select"
                value={selectedSortOptionQuery || ""}
                onChange={(e) => {
                  setSelectedSortOptionQuery(e.target.value);
                }}
              >
                {sortOptions.map((sortOption) => {
                  return (
                    <option key={sortOption.value} value={sortOption.value}>
                      {sortOption.label}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>

          <InfiniteScroll
            dataLength={events.length}
            next={paginate}
            hasMore={areAllEventsLoaded()}
            loader={<h4>Loading...</h4>}
            endMessage={<p style={{ textAlign: "center" }}></p>}
          >
            {events.length > 0 &&
              events.map((event, index) => {
                return (
                  <EventTile
                    key={index}
                    eventDetails={event}
                    onClickCallback={goToEvent}
                  />
                );
              })}
          </InfiniteScroll>
          {events.length === 0 && (
            <NoEventsTile
              text={"No events found! Please reset your filters"}
            ></NoEventsTile>
          )}
        </div>
      </div>
    </>
  );
};
