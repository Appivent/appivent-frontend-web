import React from "react";
import { useHistory } from "react-router-dom";
import "./FriendsPage.css";
import { useSelector, useDispatch } from "react-redux";
import Button from "../../components/Button/Button";
import ProfileTile from "../../components/ProfileTile/ProfileTile";
import LoadingOverlay from "../../components/LoadingOverlay/LoadingOverlay";
import { useAuth } from "../../hooks/use-auth";
import useDebounce from "../../hooks/use-debounce";
import {
  useGetFriendsList,
  useSearchFriends,
  useSearchUsers,
} from "../../hooks/user/use-user";

export const FriendsPage = () => {
  const dispatch = useDispatch();
  const friendsList = useSelector((state) => state.user.friendsList);
  const searchedFriends = useSelector((state) => state.user.searchedFriends);
  const searchedUsers = useSelector((state) => state.user.searchedUsers);
  const showSearchingUsersOverlay = useSelector(
    (state) => state.user.searchedUsersListIsLoading
  );
  const showSearchingFriendsOverlay = useSelector(
    (state) => state.user.searchedFriendsListIsLoading
  );

  const auth = useAuth();
  const history = useHistory();
  const [authUserId, setAuthUserId] = React.useState("");
  const [authJwt, setAuthJwt] = React.useState("");
  const [showFindFriendsOverlay, setShowFindFriendsOverlay] =
    React.useState(false);

  const [friendsQuery, setFriendsQuery] = React.useState("");
  const [usersQuery, setUsersQuery] = React.useState("");

  const { refetch: refetchFriendsList } = useGetFriendsList({
    lifecycleNotifications: { error: "Unable to list friends" },
  });

  const debouncedSearchFriendsQuery = useDebounce(friendsQuery, 600);
  const { refetch: refetchSearchFriends } = useSearchFriends({
    lifecycleNotifications: { error: "Unable to search friends" },
    query: { query: debouncedSearchFriendsQuery },
  });

  const debouncedSearchUsersQuery = useDebounce(usersQuery, 600);
  const { refetch: refetchSearchUsers } = useSearchUsers({
    lifecycleNotifications: { error: "Unable to search users" },
    query: { query: debouncedSearchUsersQuery },
  });

  React.useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      const thisAuthUserId = auth.userId;

      setAuthJwt(thisAuthJwt);
      setAuthUserId(thisAuthUserId);

      if (!thisAuthJwt) {
        history.push("/");
      }
    };
    getAuthInfo();
  }, []);

  const findFriends = async (query) => {
    refetchSearchFriends();
  };

  const findUsers = async (query) => {
    refetchSearchUsers();
  };

  return (
    <div className="global-middle-section">
      {showFindFriendsOverlay && (
        <div className="overlay-container">
          <div
            className="overlay"
            onClick={() => {
              setShowFindFriendsOverlay(false);
            }}
          ></div>
          <div className="find-users-container">
            {showSearchingUsersOverlay && <LoadingOverlay text={""} />}
            <h3>Add Friends</h3>
            <input
              className="find-friends-input"
              placeholder="Search..."
              value={usersQuery}
              onChange={(e) => {
                setUsersQuery(e.target.value);
                if (e.target.value.length > 3) {
                  findUsers(e.target.value);
                }
              }}
            />
            {searchedUsers.map((user, index) => {
              return <ProfileTile key={index} user={user} onXClicked={null} />;
            })}
          </div>
        </div>
      )}
      <div className="find-friends-container">
        <div className="find-friends-wrapper">
          <div className="find-friends-header-container">
            {showSearchingFriendsOverlay && <LoadingOverlay text={""} />}
            <form
              onSubmit={(e) => {
                e.preventDefault();
              }}
            >
              <input
                className="find-friends-input"
                placeholder="Search..."
                value={friendsQuery}
                onChange={(e) => {
                  setFriendsQuery(e.target.value);
                  if (e.target.value.length > 3) {
                    findFriends(e.target.value);
                  }
                }}
              />
              <Button
                text={"Find Friends"}
                styleOverrides={{ backgroundColor: "var(--light-blue)" }}
                onClickCallback={() => {
                  setShowFindFriendsOverlay(true);
                }}
              />
            </form>
          </div>
          {!friendsQuery &&
            friendsList.map((friend, index) => {
              return (
                <ProfileTile key={index} user={friend} onXClicked={null} />
              );
            })}
          {friendsQuery &&
            searchedFriends.map((friend, index) => {
              return (
                <ProfileTile key={index} user={friend} onXClicked={null} />
              );
            })}
        </div>
      </div>
    </div>
  );
};
