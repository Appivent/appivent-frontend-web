import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./ProfilePage.css";
import uploadIcon from "../../assets/upload-icon.svg";
import { useSelector, useDispatch } from "react-redux";
import { useQuery } from "../../helpers/router-helper";
import Button from "../../components/Button/Button";
import ImageUploadOverlay from "../../components/ImageUploadOverlay/ImageUploadOverlay";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {
  minimumAgeDate,
  dateToDdmmyyyyString,
  dateStringDdmmyyyToMmddyyyString,
  unixTimestampToReadableDate,
} from "../../helpers/utils";
import LoadingOverlay from "../../components/LoadingOverlay/LoadingOverlay";
import { useAuth } from "../../hooks/use-auth";
import {
  useAddFriend,
  useDeleteAccount,
  useGetUser,
  useRemoveFriend,
  useRemoveProfilePic,
  useUpdateProfilePic,
  useUpdateUserDetails,
} from "../../hooks/user/use-user";

const MODES = {
  view: "view",
  create: "create",
  edit: "edit",
};

export const ProfilePage = () => {
  const selectedUserProfile = useSelector(
    (state) => state.user.selectedUserProfile
  );
  const personalProfile = useSelector((state) => state.user.personalProfile);
  const showLoadingOverlay = useSelector(
    (state) =>
      state.user.selectedUserProfile.isLoading ||
      state.user.personalProfile.isLoading
  );
  const showUpdatingProfileOverlay = useSelector(
    (state) => state.user.personalProfile.updatingProfile
  );
  const showAddingFriendOverlay = useSelector(
    (state) => state.user.addingFriend
  );
  const showUpdatingProfilePicOverlay = useSelector(
    (state) => state.user.updatingProfilePic
  );
  const showRemovingFriendOverlay = useSelector(
    (state) => state.user.removingFriend
  );
  const showRemovingProfilePicOverlay = useSelector(
    (state) => state.user.removingProfilePic
  );
  const showDeleteAccountOverlay = useSelector(
    (state) => state.user.deletingAccount
  );
  const successfullyUpdatedProfile = useSelector(
    (state) => state.user.personalProfile.updated
  );
  const successfullyAddedFriend = useSelector(
    (state) => state.user.friendAdded
  );
  const successfullyRemovedFriend = useSelector(
    (state) => state.user.friendRemoved
  );
  const successfullyUpdatedProfilePic = useSelector(
    (state) => state.user.updatedProfilePic
  );
  const successfullyRemovedProfilePic = useSelector(
    (state) => state.user.removedProfilePic
  );
  const successfullyDeletedAccount = useSelector(
    (state) => state.user.accountDeleted
  );

  const history = useHistory();
  const auth = useAuth();
  const query = useQuery();
  const [authUserId, setAuthUserId] = React.useState("");
  const [authJwt, setAuthJwt] = React.useState("");
  const userId = query.get("userId");
  const modeQueryParam = query.get("mode") || "view";
  const [isOwnProfile, setIsOwnProfile] = React.useState(false);
  const [errorMsg, setErrorMsg] = React.useState("");
  const [mode] = React.useState(MODES[modeQueryParam]);

  const [firstName, setFirstName] = React.useState("");
  const [lastName, setLastName] = React.useState("");
  const [dateOfBirth, setDateOfBirth] = React.useState(null as any);
  const [profilePicUrl, setProfilePicUrl] = React.useState(null as any);
  const [dateTimeCreated, setDateTimeCreated] = React.useState(null as any);
  const [isPrivate, setIsPrivate] = React.useState(false);
  const [friendRequested, setFriendRequested] = React.useState(false);
  const [isFriend, setIsFriend] = React.useState(false);
  const [currentPassword, setCurrentPassword] = React.useState("");
  const [newPassword, setNewPassword] = React.useState("");

  const [isImgUploadOverlay, setIsImgUploadOverlay] = useState(false);

  const { mutate: deleteAccountMutation } = useDeleteAccount({
    lifecycleNotifications: {
      success: "Succesfully deleted account",
      error: "Unable to delete account",
    },
  });

  const { mutate: updateUserDetailsMutation } = useUpdateUserDetails({
    lifecycleNotifications: {
      success: "Succesfully updated",
      error: "Unable to update",
    },
  });

  const { mutate: updateProfilePicMutation } = useUpdateProfilePic({
    lifecycleNotifications: {
      success: "Succesfully updated",
      error: "Unable to update",
    },
  });

  const { mutate: addFriendMutation } = useAddFriend({
    lifecycleNotifications: {
      success: "Succesfully added",
      error: "Unable to add",
    },
  });

  const { mutate: removeFriendMutation } = useRemoveFriend({
    lifecycleNotifications: {
      success: "Succesfully removed",
      error: "Unable to remove",
    },
  });

  const { mutate: removeProfilePicMutation } = useRemoveProfilePic({
    lifecycleNotifications: {
      success: "Succesfully removed",
      error: "Unable to remove",
    },
  });

  const { refetch: refetchSelectedUserProfile } = useGetUser({
    lifecycleNotifications: { error: "Unable to find user details" },
    query: { userId },
  });

  React.useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      const thisAuthUserId = auth.userId;
      let thisIsOwnProfile = userId ? thisAuthUserId === userId : true;

      setAuthJwt(thisAuthJwt);
      setAuthUserId(thisAuthUserId);
      setIsOwnProfile(thisIsOwnProfile);

      if (!thisAuthJwt) {
        history.push("/");
      }
    };
    getAuthInfo();
  }, []);

  //Setting your friends/other users profile data
  React.useEffect(() => {
    if (Object.keys(selectedUserProfile.userDetails).length > 0) {
      const dateStringMmddyyyy = dateStringDdmmyyyToMmddyyyString(
        selectedUserProfile.userDetails.dateOfBirth
      );
      setFirstName(selectedUserProfile.userDetails.firstName);
      setLastName(selectedUserProfile.userDetails.lastName);
      setDateOfBirth(new Date(dateStringMmddyyyy));
      setIsPrivate(selectedUserProfile.userDetails.isPrivate);
      setProfilePicUrl(selectedUserProfile.userDetails.profilePicUrl);
      setDateTimeCreated(selectedUserProfile.userDetails.dateTimeCreated);
      setIsFriend(selectedUserProfile.isFriend);
      setFriendRequested(selectedUserProfile.friendRequested);
    }
  }, [selectedUserProfile]);

  React.useEffect(() => {
    if (
      successfullyUpdatedProfile ||
      successfullyUpdatedProfilePic ||
      successfullyRemovedProfilePic ||
      successfullyAddedFriend ||
      successfullyRemovedFriend
    ) {
      history.go(0);
    } else if (successfullyDeletedAccount) {
      logout();
    }
  }, [
    successfullyUpdatedProfile,
    successfullyAddedFriend,
    successfullyRemovedFriend,
    successfullyUpdatedProfilePic,
    successfullyRemovedProfilePic,
    successfullyDeletedAccount,
  ]);

  React.useEffect(() => {
    if (Object.keys(personalProfile.userDetails).length > 0) {
      if (!userId || userId === authUserId) {
        setPersonalProfileDetails();
      }
    }
  }, [personalProfile]);

  React.useEffect(() => {
    setErrorMsg("");
  }, [
    firstName,
    lastName,
    dateOfBirth,
    isPrivate,
    currentPassword,
    newPassword,
  ]);

  const setPersonalProfileDetails = () => {
    const dateStringMmddyyyy = dateStringDdmmyyyToMmddyyyString(
      personalProfile.userDetails.dateOfBirth
    );
    const profilePicUrlWithUnixTimestamp =
      personalProfile.userDetails.profilePicUrl + "?" + +new Date();
    setFirstName(personalProfile.userDetails.firstName);
    setLastName(personalProfile.userDetails.lastName);
    setDateOfBirth(new Date(dateStringMmddyyyy));
    setIsPrivate(personalProfile.userDetails.isPrivate);
    setProfilePicUrl(profilePicUrlWithUnixTimestamp);
    setDateTimeCreated(personalProfile.userDetails.dateTimeCreated);
  };

  const logout = async () => {
    await auth.logout();
    history.push("/");
  };

  const addOrRemoveFriend = async (add) => {
    if (add) {
      addFriendMutation({ query: { userId } });
    } else {
      removeFriendMutation({ query: { userId } });
    }
  };

  const removeProfileImage = async (e) => {
    removeProfilePicMutation({});
  };

  const goToPageWithMode = (mode) => {
    history.push({
      pathname: "/profile",
      search: "?&mode=" + mode,
    });
    history.go(0);
  };

  const goToUsersEvents = () => {
    const userIdToNavTo = userId || authUserId;
    history.push({
      pathname: "/home",
      search: "?userId=" + userIdToNavTo,
    });
    history.go(0);
  };

  const isProfileValid = (userDetails) => {
    let isValid = true;
    setErrorMsg("");

    if (
      !userDetails.firstName ||
      !userDetails.lastName ||
      !userDetails.dateOfBirth
    ) {
      isValid = false;
      setErrorMsg("Please fill incomplete fields");
    }

    return isValid;
  };

  const updateUserDetails = async () => {
    const userDetails = {
      firstName,
      lastName,
      dateOfBirth: dateToDdmmyyyyString(dateOfBirth),
      isPrivate,
    };

    if (isProfileValid(userDetails)) {
      updateUserDetailsMutation({ query: { authUserId, userDetails } });
    }
  };

  const toggleImgUploadOverlay = () => {
    setIsImgUploadOverlay(!isImgUploadOverlay);
  };

  const onChangePassword = async () => {
    let res = await auth.changePassword(currentPassword, newPassword);
    if (res.message) {
      setErrorMsg(res.message);
    } else {
      goToPageWithMode(MODES.view);
    }
  };

  return (
    <>
      {isImgUploadOverlay && (
        <ImageUploadOverlay
          updateProfilePic={updateProfilePicMutation}
          toggleImgUploadOverlay={toggleImgUploadOverlay}
        />
      )}

      {errorMsg && <>{errorMsg}</>}

      <div className="profile-card">
        {showLoadingOverlay ||
        showUpdatingProfileOverlay ||
        showAddingFriendOverlay ||
        showRemovingFriendOverlay ||
        showUpdatingProfilePicOverlay ||
        showRemovingProfilePicOverlay ||
        showDeleteAccountOverlay ? (
          <LoadingOverlay text={""} />
        ) : null}
        {mode === MODES.view && isPrivate && (
          <div className="private-status">Private</div>
        )}
        {mode === MODES.view && !isPrivate && (
          <div className="public-status">Public</div>
        )}

        {!showLoadingOverlay &&
          !showUpdatingProfileOverlay &&
          !showAddingFriendOverlay &&
          !showRemovingFriendOverlay &&
          !showUpdatingProfilePicOverlay &&
          !showRemovingProfilePicOverlay &&
          !showDeleteAccountOverlay &&
          isOwnProfile &&
          profilePicUrl && (
            <div
              className="profile-img-overlay"
              onClick={() => toggleImgUploadOverlay()}
            >
              <img
                src={uploadIcon}
                alt="Upload Icon"
                className="upload-icon"
              ></img>
            </div>
          )}
        {profilePicUrl && (
          <img
            className="profile-img"
            src={profilePicUrl}
            alt={firstName + " Profile Image"}
          ></img>
        )}

        {mode === MODES.view && (
          <>
            <h1 style={{ textAlign: "center" }}>
              {firstName + " " + lastName}
            </h1>
          </>
        )}
        {mode === MODES.view && (
          <>
            <p>Member since: {unixTimestampToReadableDate(dateTimeCreated)}</p>
          </>
        )}
        {mode === MODES.view && (
          <>
            <p>Birthday: {dateToDdmmyyyyString(dateOfBirth)}</p>
          </>
        )}

        {isOwnProfile && mode === MODES.edit && (
          <div className="edit-section-container">
            <div className="image-edit-section">
              <Button
                text="Upload Profile Picture"
                styleOverrides={{
                  backgroundColor: "var(--facebook-blue)",
                  margin: "0 5px",
                }}
                onClickCallback={() => toggleImgUploadOverlay()}
              />
              <Button
                text="Remove profile pic"
                styleOverrides={{
                  backgroundColor: "var(--light-orange)",
                  margin: "0 5px",
                }}
                onClickCallback={removeProfileImage}
              />
            </div>
          </div>
        )}

        {isOwnProfile && mode === MODES.edit && (
          <>
            <div className="profile-edit-row profile-edit-left-right-section">
              <div className="profile-edit-left-section">
                <label className={"profile-edit-row-label"} htmlFor="firstName">
                  First Name
                </label>
                <input
                  id="firstName"
                  className="profile-edit-input"
                  type="text"
                  placeholder="First Name"
                  defaultValue={firstName}
                  onChange={(e) => {
                    setFirstName(e.target.value);
                  }}
                />
              </div>
              <div className="profile-edit-right-section">
                <label className={"profile-edit-row-label"} htmlFor="lastName">
                  Last Name
                </label>
                <input
                  id="lastName"
                  className="profile-edit-input"
                  type="text"
                  placeholder="Last Name"
                  defaultValue={lastName}
                  onChange={(e) => {
                    setLastName(e.target.value);
                  }}
                />
              </div>
            </div>

            <div className="profile-edit-row profile-edit-left-right-section">
              <div className="profile-edit-left-section">
                <label className={"profile-edit-row-label"} htmlFor="birthday">
                  Birthday
                </label>
                {dateOfBirth && (
                  <DatePicker
                    wrapperClassName={"profile-edit-input"}
                    id="birthday"
                    selected={new Date(dateOfBirth)}
                    onChange={(date) => setDateOfBirth(date)}
                    dateFormat="dd/MM/yyyy"
                    maxDate={minimumAgeDate()}
                    showYearDropdown
                    dateFormatCalendar="MMMM"
                    yearDropdownItemNumber={15}
                    scrollableYearDropdown
                  />
                )}
              </div>
              <div className="profile-edit-right-section">
                <label className={"profile-edit-row-label"}>
                  Set To Private
                </label>
                <input
                  type="checkbox"
                  checked={isPrivate}
                  onChange={() => {
                    setIsPrivate(!isPrivate);
                  }}
                />
              </div>
            </div>

            <h3 style={{ width: "100%" }}>Change Password</h3>

            <div className="profile-edit-row profile-edit-left-right-section">
              <div className="profile-edit-left-section">
                <label
                  className={"profile-edit-row-label"}
                  htmlFor="currentPassword"
                >
                  Current Password
                </label>
                <input
                  id="currentPassword"
                  className="profile-edit-input"
                  type="text"
                  placeholder="Current Password"
                  defaultValue={currentPassword}
                  onChange={(e) => {
                    setCurrentPassword(e.target.value);
                  }}
                />
                <Button
                  text="Change Password"
                  styleOverrides={{
                    backgroundColor: "var(--dark-blue)",
                    margin: "0px 5px",
                    width: "110px",
                    marginTop: "10px",
                  }}
                  onClickCallback={() => {
                    onChangePassword();
                  }}
                />
              </div>
              <div className="profile-edit-right-section">
                <label
                  className={"profile-edit-row-label"}
                  htmlFor="newPassword"
                >
                  New Password
                </label>
                <input
                  id="newPassword"
                  className="profile-edit-input"
                  type="text"
                  placeholder="New Password"
                  defaultValue={newPassword}
                  onChange={(e) => {
                    setNewPassword(e.target.value);
                  }}
                />
              </div>
            </div>
          </>
        )}

        <div className="profile-buttons-container">
          {mode !== MODES.edit && isFriend && (
            <Button
              text="View Events"
              styleOverrides={{
                backgroundColor: "var(--dark-blue)",
                margin: "0 5px",
              }}
              onClickCallback={() => {
                goToUsersEvents();
              }}
            />
          )}
          {isOwnProfile && mode === MODES.view && (
            <Button
              text={"Edit Profile"}
              styleOverrides={{
                backgroundColor: "var(--dark-blue)",
                margin: "0 5px",
              }}
              onClickCallback={function () {
                goToPageWithMode(MODES.edit);
              }}
            ></Button>
          )}
          {isOwnProfile && (
            <Button
              text="Logout"
              styleOverrides={{
                backgroundColor: "var(--dark-orange)",
                margin: "0 5px",
              }}
              onClickCallback={logout}
            />
          )}
          {!isOwnProfile && !isFriend && !friendRequested && (
            <Button
              text="Add Friend"
              styleOverrides={{
                backgroundColor: "var(--dark-orange)",
                margin: "0 5px",
              }}
              onClickCallback={() => {
                addOrRemoveFriend(true);
              }}
            />
          )}
          {!isOwnProfile && isFriend && (
            <Button
              text="Remove Friend"
              styleOverrides={{
                backgroundColor: "var(--light-orange)",
                margin: "0 5px",
              }}
              onClickCallback={() => {
                addOrRemoveFriend(false);
              }}
            />
          )}
          {isOwnProfile && mode === MODES.edit && (
            <Button
              text="Save"
              styleOverrides={{
                backgroundColor: "var(--dark-blue)",
                margin: "0 5px",
              }}
              onClickCallback={updateUserDetails}
            />
          )}
          {isOwnProfile && mode === MODES.edit && (
            <Button
              text="Delete account"
              styleOverrides={{
                backgroundColor: "var(--dark-orange)",
                margin: "0 5px",
              }}
              onClickCallback={() => {
                deleteAccountMutation({});
              }}
            />
          )}
        </div>
      </div>
    </>
  );
};
