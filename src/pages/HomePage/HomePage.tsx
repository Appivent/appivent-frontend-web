import React from "react";
import { useHistory } from "react-router-dom";
import "./HomePage.css";
import { useQuery } from "../../helpers/router-helper";
import FilterPanel from "../../components/FilterPanel/FilterPanel";
import Button from "../../components/Button/Button";
import EventTile from "../../components/EventTile/EventTile";
import NoEventsTile from "../../components/NoEventsTile/NoEventsTile";
import {
  dateToUnixTimestampToString,
  unixTimestampStringToDate,
} from "../../helpers/utils";
import { useGetEventsAndFilters } from "../../hooks/event/use-event";
import { useAuth } from "../../hooks/use-auth";
import { useGetNotifications } from "../../hooks/user/use-user";

// Don't bother paginating user's Homepage events, just give all
const MAX_LIMIT = 9999;

export const HomePage = () => {
  const auth = useAuth();
  const history = useHistory();
  const query = useQuery();
  const userId = query.get("userId");
  const [authUserId, setAuthUserId] = React.useState("");
  const [offset, setOffset] = React.useState(0);
  const [totalEvents, setTotalEvents] = React.useState(0);

  const [selectedSortOptionQuery, setSelectedSortOptionQuery] =
    React.useState("stdesc");
  const [locationFilterQuery, setLocationFilterQuery] = React.useState("");
  const [tagsFilterQuery, setTagsFilterQuery] = React.useState([]);
  const [fromDateFilterQuery, setFromDateFilterQuery] = React.useState(
    new Date()
  );
  const [toDateFilterQuery, setToDateFilterQuery] = React.useState(new Date());
  const [tagsQuery, setTagsQuery] = React.useState([]);

  const [hostingEvents, setHostingEvents] = React.useState([]);
  const [attendingEvents, setAttendingEvents] = React.useState([]);
  const [isOwnHome, setIsOwnHome] = React.useState(false);

  const { refetch: refetchGetEvents } = useGetEventsAndFilters({
    lifecycleNotifications: { error: "Unable to get events" },
    query: {
      offset,
      limit: MAX_LIMIT,
      selectedSortOptionQuery,
      locationFilterQuery,
      tagsQuery,
      fromDate: dateToUnixTimestampToString(fromDateFilterQuery),
      toDate: dateToUnixTimestampToString(toDateFilterQuery),
      userId,
    },
    onSuccessCallback: (res) => {
      //TODO: Set all filter states from this response
      //TODO: Set totalEvents
    },
  });

  const { data: notifications, refetch: refetchGetNotifications } = useGetNotifications({
    lifecycleNotifications: { error: "Unable to search friends" },
  });

  React.useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      const thisAuthUserId = auth.userId;
      const fromDateUnixString =
        dateToUnixTimestampToString(fromDateFilterQuery);
      let thisIsOwnHome = userId ? thisAuthUserId === userId : true;

      setAuthUserId(thisAuthUserId);

      if (!thisAuthJwt) {
        history.push("/");
      }
      setOffset(0);
      setIsOwnHome(thisIsOwnHome);
    };
    getAuthInfo();
  }, []);

  const filterHostingEvents = (events) => {
    return events.filter((event) => event.adminId === authUserId);
  };

  const filterAttendingEvents = (events) => {
    return events.filter((event) => event.admin !== authUserId);
  };

  const filterPendingEvents = (events) => {
    return events.filter((event) => event.admin === authUserId);
  };

  const createEventHandler = () => {
    history.push({
      pathname: "/update-event",
      search: "?mode=create",
    });
  };

  const goToEvent = (eventId) => {
    history.push({
      pathname: "/event",
      search: "?eventId=" + eventId + "&mode=view",
    });
  };

  const clearFilters = () => {
    setLocationFilterQuery("");
    setTagsFilterQuery([]);
    setFromDateFilterQuery(new Date());
    setToDateFilterQuery(new Date());
    //TODO: reset filters here;
    refetchGetEvents();
  };

  return (
    <>
      <FilterPanel
        setLocationFilterQuery={setLocationFilterQuery}
        setTagsFilterQuery={setTagsFilterQuery}
        setFromDateFilterQuery={setFromDateFilterQuery}
        setToDateFilterQuery={setToDateFilterQuery}
        tagsFilterQuery={tagsFilterQuery}
        fromDateFilterQuery={fromDateFilterQuery}
        toDateFilterQuery={toDateFilterQuery}
        totalEvents={totalEvents}
        clearFilters={clearFilters}
      />
      <div className="events-container">
        <div className="events-list-container">
          {isOwnHome && (
            <>
              <div className="events-list-header">
                <div className="events-list-header-left">
                  <h3 style={{ marginRight: "10px" }}>Hosting</h3>
                  <Button
                    text="Create Event"
                    styleOverrides={{
                      backgroundColor: "var(--dark-blue)",
                      verticalAlign: "middle",
                    }}
                    onClickCallback={createEventHandler}
                  ></Button>
                </div>
              </div>
              {hostingEvents.length > 0 ? (
                hostingEvents.map((event, index) => {
                  return (
                    <EventTile
                      key={index}
                      eventDetails={event}
                      onClickCallback={goToEvent}
                      showComments={true}
                    />
                  );
                })
              ) : (
                <NoEventsTile
                  text={"You are not hosting any events"}
                ></NoEventsTile>
              )}
            </>
          )}

          <div className="events-list-header">
            {isOwnHome ? <h3>Attending</h3> : <h3>Friend's Events</h3>}
          </div>
          {attendingEvents.length > 0 ? (
            attendingEvents.map((event, index) => {
              return (
                <EventTile
                  key={index}
                  eventDetails={event}
                  onClickCallback={goToEvent}
                  showComments={true}
                />
              );
            })
          ) : (
            <NoEventsTile text={"You are attending no events"}></NoEventsTile>
          )}

          {isOwnHome && (
            <>
              <div className="events-list-header">
                <h3>Pending Invitations</h3>
              </div>
              {notifications?.eventNotifications.length > 0 ? (
                notifications?.eventNotifications.map((event, index) => {
                  return (
                    <EventTile
                      key={index}
                      eventDetails={event}
                      onClickCallback={goToEvent}
                      showComments={true}
                    />
                  );
                })
              ) : (
                <NoEventsTile
                  text={"You have no new invitations"}
                ></NoEventsTile>
              )}
            </>
          )}
        </div>
      </div>
    </>
  );
};
