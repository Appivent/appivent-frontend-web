import React from "react";
import { useHistory } from "react-router-dom";
import "./CreateEditEventPage.css";
import { useSelector, useDispatch } from "react-redux";
import { useQuery } from "../../helpers/router-helper";
import Button from "../../components/Button/Button";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {
  dateToUnixTimestampToString,
  unixTimestampStringToDate,
} from "../../helpers/utils";
import LoadingOverlay from "../../components/LoadingOverlay/LoadingOverlay";
import {
  MAX_TITLE_LENGTH,
  MAX_EVENT_DESCRIPTION_LENGTH,
  MAX_TAG_LENGTH,
} from "../../helpers/validation";
import Autocomplete from "react-autocomplete";
import {
  useCreateEvent,
  useDeleteEvent,
  useDeleteEventPic,
  useGetEvent,
  useGetLocations,
  useUpdateEventPic,
} from "../../hooks/event/use-event";
import useDebounce from "../../hooks/use-debounce";
import { useAuth } from "../../hooks/use-auth";

const MODES = {
  create: "create",
  edit: "edit",
};

export const CreateEditEventPage = () => {
  const auth = useAuth();
  const history = useHistory();
  const query = useQuery();
  const eventId = query.get("eventId");
  const modeQueryParam = query.get("mode") || "create";
  const [authUserId, setAuthUserId] = React.useState("");
  const [authJwt, setAuthJwt] = React.useState("");
  const dispatch = useDispatch();

  const eventDetails = useSelector((state) => state.events.selectedEvent);
  const searchedForLocations = useSelector(
    (state) => state.events.searchedLocations
  );
  const newleyCreatedEventId = useSelector(
    (state) => state.events.newleyCreatedEventId
  );
  const showDeletingEventPicOverlay = useSelector(
    (state) => state.events.deletingEventPic
  );
  const showUpdatingEventPicOverlay = useSelector(
    (state) => state.events.updatingEventPic
  );
  const showAttendingEventOverlay = useSelector(
    (state) => state.events.joiningEvent
  );
  const showUnattendingEventOverlay = useSelector(
    (state) => state.events.leavingEvent
  );
  const showLoadingEventOverlay = useSelector(
    (state) => state.events.eventDetailsLoading
  );
  const showCreatingEventOverlay = useSelector(
    (state) => state.events.creatingNewEvent
  );
  const showUpdatingEventOverlay = useSelector(
    (state) => state.events.updatingEvent
  );
  const showDeletingEventOverlay = useSelector(
    (state) => state.events.deletingEvent
  );
  const createdNewEvent = useSelector((state) => state.events.createdNewEvent);
  const successfullyUpdated = useSelector(
    (state) => state.events.deletingEventPic
  );
  const successfullyDeleted = useSelector((state) => state.events.deletedEvent);
  const successfullyDeletedEventPic = useSelector(
    (state) => state.events.eventPicDeleted
  );
  const successfullyUpdatedEventPic = useSelector(
    (state) => state.events.eventPicUpdated
  );

  const [title, setTitle] = React.useState("");
  const [startTime, setStartTime] = React.useState(null as any);
  const [endTime, setEndTime] = React.useState(null as any);
  const [allowGuestInvites, setAllowGuestInvites] = React.useState(false);
  const [isPrivate, setIsPrivate] = React.useState(false);
  const [description, setDescription] = React.useState("");
  const [tags, setTags] = React.useState([] as any);

  const [tagInput, setTagInput] = React.useState([] as any);
  const [googleLocations, setGoogleLocations] = React.useState([]);
  const [locationQuery, setLocationQuery] = React.useState("");
  const [selectedLocationName, setSelectedLocationName] = React.useState("");
  const [selectedLocationPlaceId, setSelectedLocationPlaceId] =
    React.useState("");
  const [attendingOrPendingUserIds, setAttendingOrPendingUserIds] =
    React.useState([] as any);

  const [mode] = React.useState(MODES[modeQueryParam]);
  const [errorMsg, setErrorMsg] = React.useState("");

  const [showDiscardEventOverlay, setShowDiscardEventOverlay] =
    React.useState(false);

  const debouncedSearchQuery = useDebounce(locationQuery, 600);
  const { refetch: refetchGetLocations } = useGetLocations({
    lifecycleNotifications: { error: "Unable to list locations" },
    query: { query: debouncedSearchQuery },
    onSuccessCallback: (res) => {
      if (searchedForLocations.length > 0) {
        setGoogleLocations(res);
      }
    },
  });

  const { refetch: refetchGetEvent } = useGetEvent({
    lifecycleNotifications: { error: "Unable to get event details" },
    query: { eventId },
    onSuccessCallback: (res) => {
      setTitle(eventDetails.title);
      setStartTime(unixTimestampStringToDate(eventDetails.startTimestamp));
      setEndTime(unixTimestampStringToDate(eventDetails.endTimestamp));
      setAllowGuestInvites(eventDetails.allowGuestInvites);
      setIsPrivate(eventDetails.isPrivate);
      setDescription(eventDetails.description);
      setSelectedLocationName(eventDetails.locationName);
      setLocationQuery(eventDetails.locationName);
      setSelectedLocationPlaceId(eventDetails.locationPlaceId);
      setTags(eventDetails.tags || []);

      if (eventDetails.attending) {
        eventDetails.attending.map((attendee) => {
          setAttendingOrPendingUserIds([
            ...attendingOrPendingUserIds,
            attendee.userId,
          ]);
        });
      }

      if (eventDetails.pending) {
        eventDetails.pending.map((pendee) => {
          setAttendingOrPendingUserIds([
            ...attendingOrPendingUserIds,
            pendee.userId,
          ]);
        });
      }
    },
  });

  const { mutate: createEventMutation } = useCreateEvent({
    lifecycleNotifications: {
      success: "Succesfully created event",
      error: "Unable to create event",
    },
  });

  const { mutate: updateEventPicMutation } = useUpdateEventPic({
    lifecycleNotifications: {
      success: "Succesfully updated event pic",
      error: "Unable to update event pic",
    },
  });

  const { mutate: deleteEventPicMutation } = useDeleteEventPic({
    lifecycleNotifications: {
      success: "Succesfully deleted event pic",
      error: "Unable to delete event pic",
    },
  });

  const { mutate: deleteEventMutation } = useDeleteEvent({
    lifecycleNotifications: {
      success: "Succesfully deleted event",
      error: "Unable to delete event",
    },
  });

  const { mutate: updateEventMutation } = useUpdateEventPic({
    lifecycleNotifications: {
      success: "Succesfully updated event",
      error: "Unable to update event",
    },
  });

  const autoCompleteMenuStyle = {
    borderRadius: "3px",
    boxShadow: "0 2px 12px rgba(0, 0, 0, 0.1)",
    background: "rgba(255, 255, 255, 0.9)",
    padding: "0",
    position: "fixed",
    overflow: "auto",
    maxHeight: "50%",
    zIndex: 99,
  };

  React.useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      const thisAuthUserId = auth.userId;

      setAuthJwt(thisAuthJwt);
      setAuthUserId(thisAuthUserId);
    };
    getAuthInfo();
  }, []);

  React.useEffect(() => {
    if (createdNewEvent) {
      goToPageWithEventId(newleyCreatedEventId);
    }
  }, [createdNewEvent]);

  React.useEffect(() => {
    if (
      successfullyUpdated ||
      successfullyDeletedEventPic ||
      successfullyUpdatedEventPic
    ) {
      goToPageWithEventId();
    }
  }, [
    successfullyUpdated,
    successfullyDeletedEventPic,
    successfullyUpdatedEventPic,
  ]);

  React.useEffect(() => {
    if (successfullyDeleted) {
      history.push({
        pathname: "/home",
      });
      history.go(0);
    }
  }, [successfullyDeleted]);

  React.useEffect(() => {
    setErrorMsg("");
  }, [
    title,
    startTime,
    endTime,
    allowGuestInvites,
    isPrivate,
    description,
    selectedLocationName,
    tagInput,
  ]);

  const goToPageWithEventId = (eventId = eventDetails.eventId) => {
    history.push({
      pathname: "/event",
      search: "?eventId=" + eventId,
    });
    history.go(0);
  };

  const isEventDataValid = (eventData) => {
    let isValid = true;
    setErrorMsg("");

    if (
      !eventData.title ||
      !eventData.description ||
      !eventData.startTimestamp ||
      !eventData.endTimestamp ||
      !eventData.locationName
    ) {
      isValid = false;
      setErrorMsg("Please fill incomplete fields");
    } else if (eventData.title > MAX_TITLE_LENGTH) {
      isValid = false;
      setErrorMsg("Title too long. Please keep this below 120 characters");
    } else if (eventData.description > MAX_EVENT_DESCRIPTION_LENGTH) {
      isValid = false;
      setErrorMsg(
        "Description too long. Please keep this below 2000 characters"
      );
    } else if (startTime > endTime) {
      isValid = false;
      setErrorMsg("Start time must be before End time");
    }

    return isValid;
  };

  const save = async () => {
    const eventData = {
      title: title,
      description: description,
      startTimestamp: dateToUnixTimestampToString(startTime),
      endTimestamp: dateToUnixTimestampToString(endTime),
      locationName: selectedLocationName,
      locationPlaceId: selectedLocationPlaceId,
      tags: tags || [],
      isPrivate: isPrivate || false,
      allowGuestInvites: allowGuestInvites || false,
    };

    if (isEventDataValid(eventData)) {
      if (mode === MODES.edit) {
        updateEventMutation({ query: { ...eventDetails, ...eventData } });
      } else if (mode === MODES.create) {
        createEventMutation({ query: { eventData } });
      }
    }
  };

  const delEvent = async () => {
    deleteEventMutation({ query: { eventId: eventDetails.eventId } });
  };

  const searchForLocation = async (query) => {
    setLocationQuery(query);
    setSelectedLocationName(query);
    setSelectedLocationPlaceId("");
    if (query.length > 3) {
      refetchGetLocations();
    }
  };

  const removeEventPic = async () => {
    deleteEventPicMutation({ query: { eventId: eventDetails.eventId } });
  };

  const saveEventImage = async (e) => {
    const file = e.target.files[0],
      reader = new FileReader();

    setErrorMsg("");

    if (file) {
      if (file.size <= 2000000) {
        reader.readAsDataURL(file);
        reader.onloadend = async () => {
          const result = reader.result;
          //@ts-ignore
          const modifiedBase64String = result.replace(
            /^data:image\/[a-z]+;base64,/,
            ""
          );
          updateEventPicMutation({
            query: {
              eventId: eventDetails.eventId,
              base64String: modifiedBase64String,
            },
          });
        };
      } else {
        setErrorMsg("File's too massive bro");
      }
    }
  };

  const handleTagsKeyDown = (e) => {
    if (e.key === "Enter") {
      if (tagInput.length > MAX_TAG_LENGTH) {
        setErrorMsg("Make sure your tag is less than 15 characters long");
      } else {
        setTags([...tags, tagInput]);
        setTagInput("");
      }
    }
  };

  const removeTag = (tagToRemove) => {
    setTags(tags.filter((e) => e !== tagToRemove));
  };

  return (
    <>
      {showDiscardEventOverlay && (
        <div className="overlay-container">
          <div
            className="overlay"
            onClick={() => {
              setShowDiscardEventOverlay(false);
            }}
          ></div>
          <div className="welcome-overlay-container">
            <h5>Discard Event?</h5>
            <div className="yes-no-buttons-container">
              <Button
                text={"Yes"}
                styleOverrides={{
                  backgroundColor: "var(--light-blue)",
                  margin: "0 5px",
                }}
                onClickCallback={function () {
                  if (Object.keys(eventDetails).length === 0) {
                    history.goBack();
                  } else {
                    delEvent();
                  }
                }}
              ></Button>
              <Button
                text={"No"}
                styleOverrides={{
                  backgroundColor: "var(--dark-orange)",
                  margin: "0 5px",
                }}
                onClickCallback={function () {
                  setShowDiscardEventOverlay(false);
                }}
              ></Button>
            </div>
          </div>
        </div>
      )}

      <div className="create-edit-event-details-container">
        <div className="create-edit-event-details-wrapper">
          {showLoadingEventOverlay ||
          showCreatingEventOverlay ||
          showUpdatingEventOverlay ||
          showDeletingEventOverlay ||
          showAttendingEventOverlay ||
          showUnattendingEventOverlay ||
          showDeletingEventPicOverlay ||
          showUpdatingEventPicOverlay ? (
            <LoadingOverlay text={""} />
          ) : null}
          <span className="back-arrow" onClick={() => history.goBack()}>
            ←
          </span>
          <h2>🎉 Create an Event</h2>

          {!authJwt && <h1>DON'T HAVE AN ACCOUNT? GET ONE TODAY HAHA</h1>}

          {mode === MODES.edit && (
            <div className="create-edit-event-detail-container">
              <label
                htmlFor="saveEventImage"
                className="create-edit-event-detail-name"
              >
                Event Cover Photo
              </label>
              <input
                id="saveEventImage"
                className="create-edit-event-detail-input"
                type="file"
                accept="image/jpeg"
                onChange={saveEventImage}
              />
              <span
                className="remove-cover-photo-link"
                onClick={removeEventPic}
              >
                Remove Cover Photo
              </span>
            </div>
          )}

          <div className="create-edit-event-detail-container">
            <label htmlFor="title" className="create-edit-event-detail-name">
              Event Name
            </label>
            <input
              id="title"
              className="create-edit-event-detail-input"
              placeholder="Title"
              value={title || ""}
              onChange={function (e) {
                setTitle(e.target.value);
              }}
            />
          </div>

          <div className="create-edit-event-detail-container create-edit-location-container">
            <label
              htmlFor="autocomplete"
              className="create-edit-event-detail-name"
            >
              Location
            </label>
            <Autocomplete
              menuStyle={autoCompleteMenuStyle}
              wrapperStyle={{ width: "100%" }}
              getItemValue={(googleLocation) => googleLocation.description}
              items={googleLocations}
              renderItem={(googleLocation, isHighlighted) => (
                <div
                  style={{
                    background: isHighlighted ? "var(--light-blue)" : "white",
                    padding: "5px",
                  }}
                >
                  {googleLocation.description}
                </div>
              )}
              value={locationQuery}
              onChange={(e) => {
                searchForLocation(e.target.value);
              }}
              onSelect={(description, googleLocation) => {
                setSelectedLocationName(description);
                setSelectedLocationPlaceId(googleLocation.place_id);
                setLocationQuery(description);
              }}
            />
          </div>

          {selectedLocationPlaceId && (
            <div className="create-edit-event-detail-container create-edit-event-map-container">
              <iframe
                width="600"
                height="300"
                frameBorder="0"
                style={{ border: "0" }}
                allowFullScreen
                src={
                  "https://www.google.com/maps/embed/v1/place?key=AIzaSyCDR3zAcLM2MSLuZ0qm0hjkudlU4VarZKs&q=place_id:" +
                  selectedLocationPlaceId
                }
              ></iframe>
            </div>
          )}

          <div className="create-edit-event-detail-container create-edit-left-right-section">
            <div className="create-edit-left-section">
              <label className="create-edit-event-detail-name">Start</label>
              <DatePicker
                className="create-edit-event-detail-input"
                selected={startTime}
                onChange={(date) => setStartTime(date)}
                selectsStart
                startDate={startTime}
                endDate={endTime}
                showYearDropdown
                dateFormatCalendar="MMMM"
                yearDropdownItemNumber={15}
                scrollableYearDropdown
                placeholderText="Start Time"
                dateFormat="dd/MM/yyyy h:mm aa"
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={15}
                timeCaption="time"
                minDate={new Date()}
              />
            </div>
            <div className="create-edit-right-section">
              <label className="create-edit-event-detail-name">End</label>
              <DatePicker
                className="create-edit-event-detail-input"
                selected={endTime}
                onChange={(date) => setEndTime(date)}
                selectsEnd
                startDate={startTime}
                endDate={endTime}
                minDate={startTime || new Date()}
                showYearDropdown
                dateFormatCalendar="MMMM"
                yearDropdownItemNumber={15}
                scrollableYearDropdown
                placeholderText="End Time"
                dateFormat="dd/MM/yyyy h:mm aa"
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={15}
                timeCaption="time"
              />
            </div>
          </div>

          <div className="create-edit-event-detail-container create-edit-left-right-section">
            <div
              className="create-edit-left-section"
              style={{ flexDirection: "row" }}
            >
              <label className="checkbox-text">Allow Others To Invite</label>
              <input
                type="checkbox"
                className="checkbox"
                checked={allowGuestInvites || false}
                onChange={function () {
                  setAllowGuestInvites(!allowGuestInvites);
                }}
              />
            </div>
            <div
              className="create-edit-right-section"
              style={{ flexDirection: "row" }}
            >
              <label className="checkbox-text">Make Event Private</label>
              <input
                type="checkbox"
                className="checkbox"
                checked={isPrivate || false}
                onChange={function () {
                  setIsPrivate(!isPrivate);
                }}
              />
            </div>
          </div>

          <div className="create-edit-event-detail-container">
            <label htmlFor="tags" className="create-edit-event-detail-name">
              Tags
            </label>
            <input
              id="tags"
              className="create-edit-event-detail-input"
              placeholder="Tags"
              value={tagInput || ""}
              onChange={function (e) {
                setTagInput(e.target.value);
              }}
              onKeyPress={handleTagsKeyDown}
            />
            {tags.length > 0 && (
              <div style={{ marginTop: "10px" }}>
                {tags.map((tag) => {
                  return (
                    <Button
                      text={tag}
                      styleOverrides={{
                        backgroundColor: "var(--light-orange)",
                        margin: "2px 2px",
                      }}
                      onClickCallback={() => {
                        removeTag(tag);
                      }}
                    />
                  );
                })}
              </div>
            )}
          </div>

          <div className="create-edit-event-detail-container">
            <label
              htmlFor="description"
              className="create-edit-event-detail-name"
            >
              Description
            </label>
            <textarea
              id="description"
              style={{ resize: "vertical" }}
              maxLength={MAX_EVENT_DESCRIPTION_LENGTH}
              rows={10}
              className="create-edit-event-detail-input"
              placeholder="Description"
              value={description || ""}
              onChange={function (e) {
                setDescription(e.target.value);
              }}
            />
          </div>

          {errorMsg && <div className="error-msg">{errorMsg}</div>}

          <div className="create-edit-event-buttons-container">
            <Button
              text={"Save"}
              styleOverrides={{
                backgroundColor: "var(--light-blue)",
                margin: "0 5px",
              }}
              onClickCallback={save}
            ></Button>
            {mode === MODES.edit && (
              <Button
                text={"Delete"}
                styleOverrides={{
                  backgroundColor: "var(--dark-orange)",
                  margin: "0 5px",
                }}
                onClickCallback={function () {
                  setShowDiscardEventOverlay(true);
                }}
              ></Button>
            )}
            {mode === MODES.create && (
              <Button
                text={"Cancel"}
                styleOverrides={{
                  backgroundColor: "var(--dark-orange)",
                  margin: "0 5px",
                }}
                onClickCallback={function () {
                  setShowDiscardEventOverlay(true);
                }}
              ></Button>
            )}
          </div>
        </div>
      </div>
    </>
  );
};
