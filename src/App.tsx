import { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { LandingPage } from "./pages/LandingPage/LandingPage";
import { ProfilePage } from "./pages/ProfilePage/ProfilePage";
import { HomePage } from "./pages/HomePage/HomePage";
import { ExplorePage } from "./pages/ExplorePage/ExplorePage";
import { EventDetailsPage } from "./pages/EventDetailsPage/EventDetailsPage";
import { CreateEditEventPage } from "./pages/CreateEditEventPage/CreateEditEventPage";
import "./App.css";
import Footer from "./components/Footer/Footer";
import { FriendsPage } from "./pages/FriendsPage/FriendsPage";
import Button from "./components/Button/Button";
import { NavBar } from "./components/NavBar/NavBar";
import { useAuth } from "./hooks/use-auth";
import { useGetUser } from "./hooks/user/use-user";

export const App = () => {
  const auth = useAuth();
  const [userId, setUserId] = useState("");
  const [toggleSignInOverlay, setToggleSignInOverlay] = useState(false);
  const [showChangeNameOverlay, setShowChangeNameOverlay] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [profilePicUrl, setProfilePicUrl] = useState("");

  const { data: userData, refetch: refetchUser } = useGetUser({
    lifecycleNotifications: { error: "Unable to get user" },
    query: { userId },
    onSuccessCallback: () => {
      const profilePicUrlWithUnixTimestamp =
        userData.profilePicUrl + "?" + +new Date();
      setFirstName(userData.firstName);
      setProfilePicUrl(profilePicUrlWithUnixTimestamp);

      if (userData.firstName === "Anony" && userData.lastName == "mous") {
        setShowChangeNameOverlay(true);
      }
    },
  });

  useEffect(() => {
    const getAuthInfo = async () => {
      const thisAuthJwt = auth.accessToken;
      const thisUserId = auth.userId;

      if (thisAuthJwt) {
        setUserId(thisUserId);
        refetchUser();
      }
    };
    getAuthInfo();
  }, []);

  useEffect(() => {
    !toggleSignInOverlay && enableVerticalScroll();
  }, [toggleSignInOverlay]);

  const toggleSignIn = () => {
    setToggleSignInOverlay(!toggleSignInOverlay);
  };

  const enableVerticalScroll = () => {
    document.body.style.overflowY = "scroll";
  };

  return (
    <Router>
      <div>
        <NavBar />

        <div className="full-height">
          {showChangeNameOverlay && (
            <div className="overlay-container">
              <div
                className="overlay"
                onClick={() => {
                  setShowChangeNameOverlay(false);
                }}
              ></div>
              <div className="welcome-overlay-container">
                <h3>Welcome to Appivent!</h3>
                <h5>Please complete your profile</h5>
                <Link className="nav-item" to="/profile?&mode=edit">
                  <Button
                    text={"My Profile"}
                    styleOverrides={{ backgroundColor: "var(--light-blue)" }}
                    onClickCallback={function () {
                      setShowChangeNameOverlay(false);
                    }}
                  ></Button>
                </Link>
              </div>
            </div>
          )}
          <Switch>
            <Route exact path="/">
              <LandingPage toggleSignIn={toggleSignIn} />
            </Route>
            {/* Doing it this way because the random key re-triggers a reload when on a homepage that's not yours, then trying to visit your own */}
            <Route
              path="/home"
              render={(props) => (
                <HomePage {...props} key={Math.floor(Date.now() / 1000)} />
              )}
            ></Route>
            <Route path="/explore">
              <ExplorePage />
            </Route>
            {/* Doing it this way because the random key re-triggers a reload when on a profile that's not yours, then trying to visit your own */}
            <Route
              path="/profile"
              render={(props) => (
                <ProfilePage {...props} key={Math.floor(Date.now() / 1000)} />
              )}
            ></Route>
            <Route path="/event">
              <EventDetailsPage />
            </Route>
            <Route path="/update-event">
              <CreateEditEventPage />
            </Route>
            <Route path="/friends">
              <FriendsPage />
            </Route>
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
};
