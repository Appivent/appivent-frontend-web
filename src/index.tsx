import * as React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import { App } from "./App";
import * as serviceWorker from "./serviceWorker";
import { ProvideAuth } from "./hooks/use-auth";
import { QueryClient, QueryClientProvider } from "react-query";
import store from "./store/store-setup";

const queryClient = new QueryClient();

ReactDOM.render(
  <React.StrictMode>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600&family=Roboto:wght@400;500;700;900&display=swap"
      rel="stylesheet"
    />
    <Provider store={store}>
      <ProvideAuth>
        <QueryClientProvider client={queryClient}>
          <App />
        </QueryClientProvider>
      </ProvideAuth>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

serviceWorker.unregister();
