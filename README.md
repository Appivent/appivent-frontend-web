# Appivent Frontend 🎉

This is the Web UI for Appivent. Built on Node v14.16.1 using React, Redux, and Sagas.

## Plugins/Deps

TypeScript - https://www.typescriptlang.org/

Redux Sagas - https://redux-saga.js.org/

React Date Picker - https://reactdatepicker.com/

Redux Toolkit - https://redux-toolkit.js.org/

React Router - https://reactrouter.com/web/guides/quick-start

Awesome Debounce Promise - https://www.npmjs.com/package/awesome-debounce-promise

AWS Amplify - https://docs.amplify.aws/lib/auth/getting-started/q/platform/js#authentication-with-amplify

## Primary Colours
* dark blue: rgb(81, 150, 166);
* light orange: rgb(242, 149, 94);
* dark orange: rgb(242, 80, 65);
* light blue: rgb(145, 215, 242);

## Validation Rules
### User Registration
First Name, Last Name, Email, Birthday, Password (Min. len. 8 char, numbers, uppercase, lowercase)

### Event Creation
Title, Start Time(Unix Timestamp), End Time(Unix Timestamp), Location, Description, Is private, Allow public invites, Description
